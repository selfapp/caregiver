import React from "react";
import { View, ScrollView, ImageBackground, StatusBar } from "react-native";

export default (BackgroundNtScroll = props => {
  return (
    <ImageBackground
    //  source={require("../../src/assets/bg.png")}
      resizeMode="cover"
      style={{ flex: 1 }}
    >
      <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated />
      {props.children}
    </ImageBackground>
  );
});
