import React, {Component}  from "react";
import { 
  TouchableOpacity,
  Image,
  View
 } from "react-native";
import {BoldText, LightText} from '../Components/styledTexts';

export class Button extends Component {
  render(){
  return (
    <TouchableOpacity
              style={[{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 40,
                  borderRadius: 22.5,
                  backgroundColor: this.props.color,
                  marginHorizontal:30 ,
                  marginVertical: 40 
                  }, this.props.style]}
                  onPress = {this.props.onPress}
          >
            {this.props.Light ? (
              <LightText style={{color: 'white'}}>{this.props.value}</LightText>
            ): (
              <BoldText style={{color: 'white'}}>{this.props.value}</BoldText>
            )
          }
              
         </TouchableOpacity>
  ) 
  }
};
export class OptionButton extends Component {
  render(){
    return (
      <View style={this.props.style}>
      <TouchableOpacity style={{ padding: 10, borderColor: 'gray', borderRadius: 5, borderWidth: 1, height: 40, flexDirection: 'row', alignItems:'center', justifyContent:'space-evenly'}}
               onPress={()=> this.props.onPress()}
            >
              <LightText style={{ color: (this.props.active) ? this.props.color : 'gray' }}>{this.props.value}</LightText>
              <Image
                style={{ marginLeft: 5 , height: 15, width: 15, tintColor: (this.props.active) ? this.props.color: 'gray'}}
                source={(this.props.active) ? require('./../assets/check.png') : require('./../assets/uncheck_button.png')}
              />
            </TouchableOpacity>
      </View>
  )
  }
};
