import React from 'react';
import {
    View,
    TouchableOpacity,
    Image,
} from 'react-native';
import {BoldText} from '../Components/styledTexts';
import colors from '../styles/colors';

export default Header = (props) => {
    return (
        <View style={{flex:1, backgroundColor:'#fff' }}>
          <View style={{ flex:1, flexDirection:'row', alignItems:'center', backgroundColor:'#fff'}}>
          <View style={{flex: 0.5}}>
            {props.leftNavigation ? (
            <TouchableOpacity
              onPress={() => {
                  props.leftNavigation.goBack(null);
              }}
            >
              <Image style={{height: 25, width: 25, marginLeft: 5, tintColor: props.color}}
                      resizeMode='contain'
                      source={require('./../assets/arrow-back.png')}
              />
            </TouchableOpacity>
            ) : (null)}
            </View>
            <View style={{ flex: 2, alignItems:'center', justifyContent:'center'}}>
            <BoldText style={{color: props.color, fontSize: 18}}>{props.value}</BoldText>
           </View>
          <View style={{flex: 0.5}}>
          {props.rightIcon ? (
            <TouchableOpacity
             onPress={()=> {
               props.onPressRight()}}
            >
              <Image style={{height: 25, width: 25, marginLeft: 5, tintColor: props.color}}
                      resizeMode='contain'
                      source={props.rightIcon}
              />
            </TouchableOpacity>
          ) : (null)}

          </View>
          </View>
        <View style={{marginTop: 5, height: 1, backgroundColor: colors.gray05}}/>
        </View>
    )
}