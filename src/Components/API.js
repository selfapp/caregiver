import { AsyncStorage } from 'react-native';
 let baseURL = 'http://13.235.21.151/';

 export default class API{

    static baseURL = baseURL;

    static request(url, method = 'GET', body = null) {
        return AsyncStorage.getItem('accessToken').then((data) => {
            let access_token = data;
            return fetch(baseURL + url, {
                method: method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + (access_token ? access_token : null)
                },
                body: body === null ? null : JSON.stringify(body)
            });
        });
    }

    

    static login(country_code, phone_number) {
        return this.request('sessions/send-otp', 'POST', {
            user: {
                contact_number: phone_number,
                country_code: country_code
            }
        });
    }
    static patientChatdetails( data) {
        return this.request(`chats/${data}`,'GET');
    }
    static syncContacts(contacts) {
        return this.request('profiles/check-numbers', 'POST', {
            contact_numbers: contacts
        });
    }
    static reSendOTP(country_code, phone_number){
        return this.request('sessions/resend-passcode', 'POST',{
            user: {
                contact_number: phone_number,
                country_code: country_code
            }
        });
    }

    static verifyOTP(code, country_code, phone_number, latitude, longitude) {
        console.log("verify otp body",  latitude, longitude)
        return this.request('sessions/verify-otp', 'POST', {
            user: {
                otp: code,
                contact_number: phone_number,
                country_code: country_code,
                lat:latitude,
                lng:longitude
            }
        });
    }

    static updatePatientProfile(first_name, last_name, gender, age_group, service, image){
        return this.request('profiles/update', 'PATCH', {
            user: {
                first_name: first_name,
                last_name: last_name,
                gender_id: gender,
                age_group_id: age_group,
                services_id: service,
                image: image,
                type:'Patient'
            }
        });
    }

    static updateServiceProviderProfile(first_name, last_name, gender, age_group, service, image, qualification, license_number, license_image, additional_certificates){
        return this.request('profiles/update', 'PATCH', {
            user: {
                first_name: first_name,
                last_name: last_name,
                gender_id: gender,
                age_group_id: age_group,
                service_ids: service,
                image: image,
                type:'ServiceProvider',
                qualification_id: qualification,
                license_id: license_number,
                license_image: license_image,
                documents_attributes: additional_certificates
            }
        });
    }
    static saveToken(token,decide){
        console.log(token)
        var body={};
        if (decide) {
            body= { user: { gcm_token : token  } };
        }else{ 
            body= { user: {  apns_token: token  } };
        }
        console.log('&&&&&&&&&&&&&&& fcm body', body)
        return this.request('profiles/update', 'PATCH', body);
    }
static updateFamilyMemberProfile(first_name, last_name, image){
    return this.request('profiles/update', 'PATCH', {
        user: {
            first_name: first_name,
            last_name: last_name,
            type: 'FamilyMember',
            image: image
        }
    });
}
static addFamilyMember(first_name, last_name, image, phone_number){
    return this.request('family-members/add-family-member', 'POST', {
        user: {
            contact_number:phone_number,
            first_name: first_name,
            last_name: last_name,
            type:'FamilyMember',
            image: image
        }
    })
}

static fetchFamilyMemberList(){
    return this.request('family-members', 'GET')
}

static addPatient(first_name, last_name, image, phone_number, gender_id, age_group_id,services_id){
    return this.request('family-members/add-patient', 'POST', {
        user: {
            contact_number:phone_number,
            first_name: first_name,
            last_name: last_name,
            type:'patient',
            gender_id: gender_id,
            age_group_id: age_group_id,
            services_id: services_id,
            image: image
        }
    })
}
static fetchFamilyPatientList(){
    return this.request('patients', 'GET')
}

static fetchServicesByLocation(lat, lng){
    return this.request('nearby-services', 'POST',{
        lat:lat,
        lng:lng
    })
}
static dutyOnOff(lat, lng, available){
    return this.request('update-duty', 'PATCH',{
        user:{
            lat:lat,
            lng:lng,
            available:available
        }
    })
}
static requestForService(serviceId, patientId, lat, lng){
    console.log("url :",'appointments',JSON.stringify({
        service:{
            id:serviceId,
            lat:lat,
            lng:lng
        },
        patient:{id:patientId}
    }))
   if(patientId){
       console.log()
    return this.request('appointments', 'POST', {
        service:{
            id:serviceId,
            lat:lat,
            lng:lng
        },
        patient:{id:patientId}
    })
   }else{
    return this.request('appointments', 'POST', {
        service:{
            id:serviceId,
            lat:lat,
            lng:lng
        },
    })
   }
}
static getAppointmentStatus(appointmentId){
    return this.request(`appointments/${appointmentId}`, 'GET')
}
static fetchAppointmentList(){
    return this.request('appointments', 'GET')
}

static acceptRejectAppointment(appointmentId, status){
    if(status===2){
        return this.request(`appointments/${appointmentId}/reject`, 'PATCH',)
        
    }else{
        return this.request(`appointments/${appointmentId}/accept`, 'PATCH',)
    }
}

static logout(){
    return this.request('logout', 'DELETE')
}
static getLoactionDetails = async (address,callback) => {
    let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
    // let response = this.request(url + address + '&key=AIzaSyBy4SEL5Madg88ynd8ZbqARpsGMeBTT1As');
    let URL= url + address + '&key=AIzaSyBy4SEL5Madg88ynd8ZbqARpsGMeBTT1As'
    await fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyBy4SEL5Madg88ynd8ZbqARpsGMeBTT1As').then(async(response) => response.json())
    .then((responseJson) => {
        if(responseJson){
if(responseJson['results'].length>0){


        
        if(responseJson['results'][0]['geometry']){
            console.log({
                location:'https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyBy4SEL5Madg88ynd8ZbqARpsGMeBTT1As',
                latitude: responseJson['results'][0]['geometry']['location']['lat'],
                longitude: responseJson['results'][0]['geometry']['location']['lng']
            })
            callback({
                latitude: responseJson['results'][0]['geometry']['location']['lat'],
                longitude: responseJson['results'][0]['geometry']['location']['lng']
            })
    }
}
}
    })
  };
//    static getAddress=  async (item)=>{
//       var item2= item
//     //   console.log(item);
//       item.map((unit,id)=>{
//         //   console.log(unit.lat,unit.lng)
//           fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+unit.lat+','+unit.lng+'&key=AIzaSyBy4SEL5Madg88ynd8ZbqARpsGMeBTT1As').then(async(response) => response.json())
//     .then((responseJson) => {
// //  console.log(responseJson);  
//  if(responseJson.status==="OK" && responseJson.results.length>0)  {
//    item2[id].address=responseJson.results[0].formatted_address,id;
//  } 
// })
//       })
     
// console.log(item2);
// return item2
   
//   }
   static fetchTrips(){
    return this.request('family-members/available-trips', 'GET')
   }
}

