import React from "react";
import { View, ScrollView, ImageBackground, StatusBar } from "react-native";
import colors from "../styles/colors";

export default (Background = props => {
  return (
    <ImageBackground
      source={require("../../src/assets/bg-img.png")}
      resizeMode="cover"
      style={{ flex: 1,backgroundColor:colors.white }}
    >
      <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated />
      <ScrollView
        scrollEventThrottle={1000}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={"handled"}
        keyboardDismissMode={"interactive"}
      >
        {props.children}
      </ScrollView>
    </ImageBackground>
  );
});
