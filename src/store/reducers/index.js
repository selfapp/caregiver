import { combineReducers } from 'redux';
import { userReducer } from './userReducer';
import { servicesReducer } from './servicesReducer';
import { TripReducer} from './TripsReduces';
export default combineReducers({
    user: userReducer,
    services: servicesReducer,
    trip:TripReducer
});