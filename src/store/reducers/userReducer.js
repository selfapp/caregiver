import * as actionTypes from '../ActionTypes';

const initialState ={
    phone_number: "",
    country_code:"+1",
    userType: null,
    uuid: null,
    first_name: null,
    last_name: null,
    gender: 1,
    age_group: 1,
    service: [1],
    profile_image: null,
    qualification: 1,
    license_number: null,
    license_image: null,
    additionalCertificates:['add'],
    loading:false,
    familyMemberList:[],
    familyPatientList:[],
    errors: {},
   appointments:null,
   patients: null,
   services: null,
   visit:null,
   isEdit: false,
   contactList:[]
}

export const userReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.TYPE_MOBILE: {
            return {
                ...state,
                phone_number: action.payload,
                errors: {}
            }
        }
        case actionTypes.CHOOSE_COUNTRY: {
            return {
                ...state,
                country_code: action.payload
            }
        }
        case actionTypes.LOGIN_START: {
            return {
                ...state,
                loading: true,
                errors: {}
            }
        }
        case actionTypes.LOGIN_SUCCESS: {
            return {
                ...state,
                loading: false,
                errors: {}
            }
        }
        case actionTypes.LOGIN_ERROR: {
            return {
                ...state,
                loading: false,
                errors: action.payload
            }
        }
        case actionTypes.LOGIN_OTP: {
            return {
                ...state,
                loading: false,
                errors: {}
            }
        }
        case actionTypes.LOGIN_FAILED: {
            return {
                ...state,
                loading: false,
                errors: {
                    message: action.payload
                }
            }
        }
        case actionTypes.OTP_SUCCESS: {
            console.log(action.payload.service_ids[0])
            if(action.payload.service_ids.length>0){
                // return{
                //     ...state,
                //     service: action.payload.service_ids,
                // }
                var service= action.payload.service_ids;
                var sservicesId=[];
                for (let index = 0; index < service.length; index++) {
                    sservicesId.push(service[index].id)
                    if(index === service.length-1){
                        return {
                            ...state,
                            loading: false,
                            // uuid: action.payload.uuid,
                            service:sservicesId,
                            phone_number: action.payload.contact_number,
                            first_name: action.payload.first_name,
                            last_name: action.payload.last_name,
                            phone_number: action.payload.contact_number,
                            userType: action.payload.type,
                            gender: action.payload.gender_id,
                            age_group: action.payload.age_group_id,
                            profile_image: action.payload.image,
                            qualification: action.payload.qualification_id,
                            license_number: action.payload.license_id,
                            license_image: action.payload.license_image,
                            appointments: action.payload.appointments,
                            patients:  action.payload.patients,
                            services:  action.payload.services,
                            visit: action.payload.visit,
                        }
                    }
                }
            }
            else{
                return {
                    ...state,
                    loading: false,
                    // uuid: action.payload.uuid,
                    phone_number: action.payload.contact_number,
                    first_name: action.payload.first_name,
                    last_name: action.payload.last_name,
                    phone_number: action.payload.contact_number,
                    userType: action.payload.type,
                    gender: action.payload.gender_id,
                    age_group: action.payload.age_group_id,
                    profile_image: action.payload.image,
                    qualification: action.payload.qualification_id,
                    license_number: action.payload.license_id,
                    license_image: action.payload.license_image,
                    appointments: action.payload.appointments,
                    patients:  action.payload.patients,
                    services:  action.payload.services,
                    visit: action.payload.visit,
                }
            }
        }
        case actionTypes.SELECT_USER_TYPE: {
            return {
                ...state,
                userType: action.payload,
            }
        }
        case actionTypes.TYPE_FIRST_NAME: {
            let errors = JSON.parse(JSON.stringify(state.errors));
            if(errors.first_name) {
                delete errors.first_name;
            }
            return {
                ...state,
                first_name: action.payload,
                errors: errors
            }
        }
        case actionTypes.TYPE_LAST_NAME: {
            let errors = JSON.parse(JSON.stringify(state.errors));
            if(errors.last_name) {
                delete errors.last_name;
            }
            return {
                ...state,
                last_name: action.payload,
                errors: errors
            }
        }
        case actionTypes.SELECT_GENDER:{
            return {
                ...state,
                gender: action.payload,
            }
        }
        case actionTypes.SELECT_AGE_GROUP:{
            return {
                ...state,
                age_group: action.payload
            }
        }
        case actionTypes.SELECT_SERVICE:{
       
                if(state.service.length===0){
     return {
                ...state,
             service: [...state.service,action.payload]
            }
                }else{
                    var service = state.service;
                    var exist = [];
                    if(service.includes(action.payload)){
                       
                        exist =   service.filter(e => e !== action.payload ); 
                        return {
                            ...state,
                         service: exist
                       }
                    }else{
                        // service.push(action.payload)
                        return {
                            ...state,
                         service: [...service, action.payload]
                       }
                    }
                }
               
        }
        case actionTypes.SELECT_PROFILE_IMAGE:{
            return {
                ...state,
                profile_image: action.payload
            }
        }
        case actionTypes.SELECT_QUALIFICATION:{
            return {
                ...state,
                qualification: action.payload
            }
        }
        case actionTypes.TYPE_LICENSE_NUMER:{
            return {
                ...state,
                license_number: action.payload
            }
        }
        case actionTypes.SELECT_LICENSE_IMAGE:{
            return {
                ...state,
                license_image: action.payload
            }
        }
        case actionTypes.SELECT_ADDITIONA_CERTIFICATE:{
            let certificate = state.additionalCertificates;
            certificate = [action.payload, ...state.additionalCertificates]
            return {
                ...state,
                additionalCertificates: certificate
            }
        }
        case actionTypes.START_UPDATE_PROFILE:{
            return {
                ...state,
                loading: true
            }
        }
        case actionTypes.END_UPDATE_PROFILE:{
            return {
                ...state,
                loading: false
            }
        }
        case actionTypes.ERROR_UPDATE_PROFILE: {
            return {
                ...state,
                loading: false,
                errors: action.payload
            }
        }
        case actionTypes.FETCH_FAMILY_MEMBER_SUCCESS:{
            return {
                ...state,
                familyMemberList:action.payload
            }
        }
        case actionTypes.FETCH_FAMILY_MEMBER_FAILED: {
            return {
                ...state,
                errors: action.payload
            }
        }
        case actionTypes.FETCH_FAMILY_PATIENT_SUCCESS:{
            return {
                ...state,
                familyPatientList:action.payload
            }
        }
        case actionTypes.FETCH_FAMILY_PATIENT_FAILED: {
            return {
                ...state,
                errors: action.payload
            }
        }
        case actionTypes.EDIT_PROFILE:{
            return{
                ...state,
                isEdit: true
            }
        }
        case actionTypes.EDIT_PROFILE_SUCCESS:{
            return{
                ...state,
                isEdit: false
            }
        }

        case actionTypes.SYNC_CONTACT_START:{
            return{...state, loading: true}
        }
        case actionTypes.SYNC_CONTACT_SUCCESS:{
            return{...state, loading: false, contactList: action.payload}
        }
        case actionTypes.SYNC_CONTACT_FAILED:{
            return{...state, loading: false}
        }

        case actionTypes.USER_LOGOUT:{
            return {
                ...state,
                phone_number: "",
                country_code:"+1",
                userType: null,
                uuid: null,
                first_name: null,
                last_name: null,
                gender: 1,
                age_group: 1,
                service: [],
                profile_image: null,
                qualification: 1,
                license_number: null,
                license_image: null,
                additionalCertificates:['add'],
                loading:false,
                familyMemberList:[],
                familyPatientList:[],
                errors: {},
                appointments:null,
                patients: null,
                services: null,
                visit:null,
                contactList:[]
            }
        }
        default:
            return state
    }
}