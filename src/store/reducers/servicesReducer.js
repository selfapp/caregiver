import * as actionTypes from '../ActionTypes';


const initialState ={
        avilableServices:[],
        appointmentList:[],
        requestAppointment:null,
        available:false,
        loading: false,
        SPChatData:null,
        pChatData:null,
        chatroom:null,
        otherparty:null
}

export const servicesReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.FETCH_SERVICES_By_LOCATION_START: {
            return {
                ...state,
                error:'',
                loading: true                
            }
        }
        case actionTypes.FETCH_SERVICES_By_LOCATION_SUCCESS: {
            return {
                ...state,
                avilableServices: action.payload,
                loading: false                
            }
        }
        case actionTypes.FETCH_SERVICES_By_LOCATION_FAILED: {
            return {
                ...state,
                loading: false                
            }
        }
        case actionTypes.DUTY_ON_OFF_REQUEST: {
            return {
                ...state,
                loading: true                
            }
        }
        case actionTypes.DUTY_ON_OFF_SUCCESS: {
            return {
                ...state,
                available:action.payload,
                loading: false                
            }
        }
        case actionTypes.DUTY_ON_OFF_FAILED: {
            return {
                ...state,
                loading: false                
            }
        }

        case actionTypes.REQUEST_SERVICE_START: {
            return {
                ...state,
                requestAppointment:null,
                loading: true                
            }
        }
        case actionTypes.REQUEST_SERVICE_SUCCESS: {
            return {
                ...state,
                requestAppointment:action.payload,
                loading: false                
            }
        }
        case actionTypes.REQUEST_SERVICE_FINISH: {
            return {
                ...state,
                requestAppointment:null,
                loading: false                
            }
        }
        case actionTypes.REQUEST_SERVICE_FAILED: {
            return {
                ...state,
                requestAppointment:null,
                loading: false                
            }
        }
        case actionTypes.APPOINTMENT_STATUS:{
            return {
                ...state,
                requestAppointment:action.payload
            }
        }
        case actionTypes.REMOVE_PREVIOUS_BOOKED:{
            return{
                ...state,
                requestAppointment:null
            }
        }

        case actionTypes.FETCH_APPOINTMENT_START: {
            return {
                ...state,
                loading: false                
            }
        }
        case actionTypes.FETCH_APPOINTMENT_SUCCESS: {
            return {
                ...state,
                appointmentList: action.payload,
                loading: false                
            }
        }
        case actionTypes.FETCH_APPOINTMENT_FAILED:{
            return {
                ...state,
                loading: false
            }
        }
        case actionTypes.UPDATE_APPOINTMENT_START: {
            return {
                ...state,
                loading: false                
            }
        }
        case actionTypes.UPDATE_APPOINTMENT_SUCCESS: {
            return {
                ...state,
                loading: false,
                SPChatData:action.payload,
                chatroom:action.payload.id, 
                otherparty:action.other    
            }
        }
        case actionTypes.UPDATE_APPOINTMENT_FAILED:{
            return {
                ...state,
                loading: false
            }
        }
        case actionTypes.PCHATDATA:{
            return{
                ...state,
                pChatData:action.payload
            }
        }

        default:{
            return {
                ...state
            }
        }
    }
}