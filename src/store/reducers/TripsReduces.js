import * as actionTypes from '../ActionTypes';
import API from '../../Components/API'

const initialState ={
        TripsList:[],
        loading: false
}

export const TripReducer = (state = initialState, action) => {
    switch(action.type){       
        case actionTypes.FETCH_TRIPS_START: {
            return {
                ...state,
                loading: true                
            }
        }
        case actionTypes.FETCH_TRIPS_SUCESS: {
            return {
                ...state,
                TripsList: action.payload,
                loading: false                
            }
        }
        case actionTypes.FETCH_TRIPS_FAILED:{
            return {
                ...state,
                loading: false
            }
        }
        
        default:{
            return {
                ...state
            }
        }
    }
}