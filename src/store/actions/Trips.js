import {
    FETCH_TRIPS_FAILED,FETCH_TRIPS_START,FETCH_TRIPS_SUCESS,
} from '../ActionTypes';
import API from '../../Components/API';


export const fetchTrips = (dispatch) =>{
    dispatch({
        type: FETCH_TRIPS_START,
    })
    API.fetchTrips()
    .then((res) => res.json())
    .then(jsonRes =>{
        console.log(jsonRes)
         if(jsonRes.status){
             console.log(jsonRes.available_trip)
dispatch({
        type: FETCH_TRIPS_SUCESS,
        payload: jsonRes.available_trip,
    })
        }else{
            dispatch({
                type: FETCH_TRIPS_FAILED
            }) 
        }
    })
    .catch(err => {
        console.log(err);
    })
}
