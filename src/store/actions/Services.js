import {
    FETCH_SERVICES_By_LOCATION_START,
    FETCH_SERVICES_By_LOCATION_SUCCESS,
    FETCH_SERVICES_By_LOCATION_FAILED,
    DUTY_ON_OFF_REQUEST,
    DUTY_ON_OFF_SUCCESS,
    DUTY_ON_OFF_FAILED,
    REQUEST_SERVICE_START,
    REQUEST_SERVICE_SUCCESS,
    REQUEST_SERVICE_FAILED,
    APPOINTMENT_STATUS,
    FETCH_APPOINTMENT_START,
    FETCH_APPOINTMENT_SUCCESS,
    FETCH_APPOINTMENT_FAILED,
    UPDATE_APPOINTMENT_START,
    UPDATE_APPOINTMENT_SUCCESS,
    UPDATE_APPOINTMENT_FAILED,
    REMOVE_PREVIOUS_BOOKED,
    REQUEST_SERVICE_FINISH,
    PCHATDATA
} from '../ActionTypes';
import API from '../../Components/API';


export const fetchServicesByLocation = (lat, lng, dispatch) =>{
    dispatch({
        type: FETCH_SERVICES_By_LOCATION_START,
    })
    console.log(lat, lng)
    API.fetchServicesByLocation(lat, lng)
    .then((res) => res.json())
    .then(jsonRes =>{
        console.log(jsonRes)
         if(jsonRes.status){
            dispatch({
                type: FETCH_SERVICES_By_LOCATION_SUCCESS,
                payload: jsonRes.service
            })
        }else{
            dispatch({
                type: FETCH_SERVICES_By_LOCATION_FAILED,
            })
            alert(jsonRes.message)
        }
    })
    .catch(err => {
        console.log(err);
    })
}

export const requestForService = (serviceId, patientId, lat, lng, dispatch) =>{
    // dispatch({
    //     type: REQUEST_SERVICE_START,
    // })
    console.log("---service request----",serviceId, patientId, lat, lng )
        API.requestForService(serviceId, patientId, lat, lng)
        .then((res) => res.json())
        .then(jsonRes =>{
            console.log("---service response----",jsonRes)
             if(jsonRes.status){
                dispatch({
                    type: REQUEST_SERVICE_SUCCESS,
                    payload: jsonRes.appointment
                })
            }else{
                dispatch({
                    type: REQUEST_SERVICE_FAILED,
                })
                alert(jsonRes.message)
            }
        })
        .catch(err => {
            console.log(err);
        })
    
   
}
export const completed_service = (dispatch) =>{
    dispatch({
        type: REQUEST_SERVICE_FINISH,
    })  
}

export const getAppointmentStatus = (appointmentId, dispatch) =>{
    console.log('get appointment status ')
    API.getAppointmentStatus(appointmentId)
    .then((res) => res.json())
    .then(jsonRes =>{
        console.log("appoinment status",appointmentId,jsonRes)
         if(jsonRes.status){
             if(jsonRes.appointment.status === "accepted"){
                API.patientChatdetails(appointmentId).then((res) => res.json()).then(
                    JSONresChat=>{
                        console.log("chat details patient",JSONresChat)
                        if(JSONresChat.status){
                            dispatch({
                                type: PCHATDATA,
                                payload: JSONresChat.records
                            })
                        }
                    }
                )
             }
           if(jsonRes.appointment.status!=="completed"){
               
            dispatch({
                type: APPOINTMENT_STATUS,
                payload: jsonRes.appointment
            })
           }else{
           
            dispatch({
                type: APPOINTMENT_STATUS,
                payload: jsonRes.appointment
            })
            dispatch({
                type: REQUEST_SERVICE_FINISH,
            })
           }
        }
    })
    .catch(err => {
        console.log(err);
    })
}


export const dutyOnOff = (lat, lng, available, dispatch) =>{
    dispatch({
        type: DUTY_ON_OFF_REQUEST,
    })
    API.dutyOnOff(lat, lng, available)
    .then((res) => res.json())
    .then(jsonRes =>{
        console.log(jsonRes)
         if(jsonRes.status){
            dispatch({
                type: DUTY_ON_OFF_SUCCESS,
                payload: jsonRes.service_provider.available
            })
        }else{
            dispatch({
                type: DUTY_ON_OFF_FAILED,
                payload: jsonRes.message
            })
        }
    })
    .catch(err => {
        console.log(err);
    })
}

export const fetchAppointmentList = (dispatch) =>{
    dispatch({
        type: FETCH_APPOINTMENT_START,
    })
    API.fetchAppointmentList()
    .then((res) => res.json())
    .then(jsonRes =>{
         if(jsonRes.status){
             console.log('jsonRes.appointments',jsonRes.appointments)
            dispatch({
                type: FETCH_APPOINTMENT_SUCCESS,
                payload: jsonRes.appointments
            })
        }else{
            dispatch({
                type: FETCH_APPOINTMENT_FAILED
            }) 
        }
    })
    .catch(err => {
        console.log(err);
    })
}

export const removePreBooked = (dispatch) =>{
    dispatch({
        type:REMOVE_PREVIOUS_BOOKED
    })
}
export const acceptRejectAppointment = ( appointmentId, status, phone, navigation, dispatch) =>{
    dispatch({
        type: UPDATE_APPOINTMENT_START,
    })
    API.acceptRejectAppointment(appointmentId, status)
    .then((res) => res.json())
    .then(jsonRes =>{
        console.log("89898988989989----------------------########################################",jsonRes)
         if(jsonRes.status){
             if(jsonRes.records.custom_data.sender.id===phone){
                dispatch({
                    type: UPDATE_APPOINTMENT_SUCCESS,
                    payload: jsonRes.records,
                    other:jsonRes.records.custom_data.receiver
                })
             }else{
                dispatch({
                    type: UPDATE_APPOINTMENT_SUCCESS,
                    payload: jsonRes.records,
                    other:jsonRes.records.custom_data.sender
                })
             }
           
            fetchAppointmentList(dispatch);
            navigation.navigate('tripDetails')
        }else{
            dispatch({
                type: UPDATE_APPOINTMENT_FAILED
            })
            alert(jsonRes.message) 
        }
    })
    .catch(err => {
        console.log(err);
    })
}
