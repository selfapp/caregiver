import{
TYPE_MOBILE,
CHOOSE_COUNTRY,
LOGIN_START,
LOGIN_ERROR,
LOGIN_OTP,
OTP_SUCCESS,
TYPE_FIRST_NAME,
TYPE_LAST_NAME,
SELECT_GENDER,
SELECT_AGE_GROUP,
SELECT_SERVICE,
SELECT_USER_TYPE,
ERROR_UPDATE_PROFILE,
START_UPDATE_PROFILE,
SELECT_PROFILE_IMAGE,
SELECT_QUALIFICATION,
TYPE_LICENSE_NUMER,
SELECT_LICENSE_IMAGE,
SELECT_ADDITIONA_CERTIFICATE,
FETCH_FAMILY_MEMBER_SUCCESS,
FETCH_FAMILY_MEMBER_FAILED,
FETCH_FAMILY_PATIENT_SUCCESS,
FETCH_FAMILY_PATIENT_FAILED,
SYNC_CONTACT_START,
SYNC_CONTACT_SUCCESS,
SYNC_CONTACT_FAILED,
USER_LOGOUT
} from '../ActionTypes';
import { StackActions, NavigationActions } from 'react-navigation';
import API from '../../Components/API';
import { AsyncStorage } from 'react-native';
import colors from '../../styles/colors';


export const typeMobile = phone_number => {
    return {
        type: TYPE_MOBILE,
        payload: phone_number
    }
};
export const chooseCountry = country_code => {
    return {
        type: CHOOSE_COUNTRY,
        payload: country_code
    }
};
export const login = (country_code, phone_number, dispatch, navigate) =>{
    if(phone_number.length === 10){
        dispatch({
            type:LOGIN_START
        });
        API.login(country_code, phone_number)
        .then(res => res.json())
        .then(jsonRes => {
            console.log('Send otp Response ', JSON.stringify(jsonRes));
            if(jsonRes.success){
                dispatch({
                    type: LOGIN_OTP
                });
                navigate('VerificationCode',{
                    phoneNumber: phone_number,
                    country_code: country_code
                });
            }else{
                dispatch({
                    type: LOGIN_ERROR,
                    payload:{
                        phone_number: [jsonRes.message]
                    }
                });
            }
        })
        .catch(err =>{
            dispatch({
                type: LOGIN_ERROR,
                payload: err
            });
        });
    }else{
        dispatch({
            type: LOGIN_ERROR,
            payload:{
                phone_number:['Kindly enter 10 digit phone number']
            }
        });
    }
};
export const verifyOTP = (code, country_code, phone_number, latitude, longitude, FCM_TOKEN, device_Type, dispatch, navigation) => {
    if (code.length === 4) {
        dispatch({
            type: LOGIN_START
        });
        console.log("-------------------------",FCM_TOKEN)
        try{
            API.verifyOTP(code, country_code, phone_number, latitude, longitude)
            .then(res => res.json())
            .then(jsonRes => {
                console.log('OTP submit response ', JSON.stringify(jsonRes))
                if(jsonRes.status){
                    AsyncStorage.setItem('accessToken', jsonRes.user.access_token)
                    
                    .then(() => {
                        console.log("payload is this",jsonRes.user)
                        dispatch({
                            type: OTP_SUCCESS,
                            payload: jsonRes.user
                        });
                        AsyncStorage.setItem('user', JSON.stringify(jsonRes.user))
                        .then(() =>{
                            if(jsonRes.user.first_name == null){
                                
                                navigation.navigate('profileCreationNavigationOptions');
                        }else{
                            
                                if(jsonRes.user.type === 'Patient'){
                                    navigation.navigate('PTabNavigator');
                                }else if(jsonRes.user.type === 'FamilyMember'){
                                    navigation.navigate('FMTabNavigator');
                                }else if(jsonRes.user.type === 'ServiceProvider'){
                                    navigation.navigate('SpTabNavigator');
                                }else{
                                    navigation.navigate('profileCreationNavigationOptions');
                                }
                        }
                        API.saveToken(FCM_TOKEN, device_Type).then(res=>res.json().then(jres=>{
                            console.log("*********************************",jres,res);
                        }))
                        })
                    })
                }else{
                    dispatch({
                        type: LOGIN_ERROR,
                        payload: {
                            otp: [jsonRes.message]
                        }
                    });
                }
            })
            .catch(err =>{
                dispatch({
                    type: LOGIN_ERROR,
                    payload: err
                });
            });
        }catch(error){
            dispatch({
                type: LOGIN_ERROR,
                payload: error
            });
        }
    } else {
        dispatch({
            type: LOGIN_ERROR,
            payload: {
                otp: ['Please enter the correct OTP']
            }
        });
    }
};
export const selectUserType = (userType, dispatch, navigation) => {
        dispatch({
            type: SELECT_USER_TYPE,
            payload: userType
        })
            if(userType === 'Patient'){
                navigation.navigate('PatientProfile');
            }else if(userType === 'family'){
                navigation.navigate('FamilyProfile');
            }else if( userType === 'ServiceProvider'){
                navigation.navigate('ServiceProviderProfile');
            }
};
export const typeFirstName = first_name => {
    return {
        type: TYPE_FIRST_NAME,
        payload: first_name
    }
};
export const typeLastName = last_name => {
    return {
        type: TYPE_LAST_NAME,
        payload: last_name
    }
};
export const selectGender = gender => {
    return {
        type: SELECT_GENDER,
        payload: gender
    }
};
export const selectAgeGroup = age_group => {
    return {
        type: SELECT_AGE_GROUP,
        payload: age_group
    }
};
export const selectService = service => {
    return {
        type: SELECT_SERVICE,
        payload: service
    }
};
export const selectProfileImage = profile_image =>{
    return{
        type: SELECT_PROFILE_IMAGE,
        payload:profile_image
    }
}
export const selectQualification =  qualification =>{
    return{
        type: SELECT_QUALIFICATION,
        payload:qualification
    }
}
export const typeLicenseNumber = license_number =>{
    return{
        type: TYPE_LICENSE_NUMER,
        payload: license_number
    }
}
export const selectLicenseImage = license_image =>{
    return{
        type: SELECT_LICENSE_IMAGE,
        payload:license_image
    }
}
export const selectAdditionalCertificate = certificate =>{
    return {
        type: SELECT_ADDITIONA_CERTIFICATE,
        payload: certificate
    }
}

export const logout =(dispatch, navigation) =>{
    API.logout()
    .then((res) => res.json())
    .then(jsonRes => {
        console.log(jsonRes)
        if(jsonRes.status){
            AsyncStorage.clear();
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'loginNavigationOptions' })],
              });
              dispatch({
                type: USER_LOGOUT
            });
            navigation.dispatch(resetAction);
        }
    })
}
//Patient 
export const updatePatientProfile = (profile_image, first_name, last_name, gender, age_group,service, dispatch, navigation) =>{
    dispatch({
        type: START_UPDATE_PROFILE
    });
    API.updatePatientProfile(first_name, last_name, gender, age_group, service, profile_image)
    .then((res) => res.json())
    .then(jsonRes => {
        if(jsonRes.status){
            console.log('update profile Response', JSON.stringify(jsonRes))
            dispatch({
                type: OTP_SUCCESS,
                payload: jsonRes.user
            })
            AsyncStorage.setItem('user', JSON.stringify(jsonRes.user))
            .then(() =>{
                navigateToPayment(colors.patient, navigation)
            });
        }else{
            dispatch({
                type: ERROR_UPDATE_PROFILE,
                payload: jsonRes.errors
            })
        }
    })
    .catch(err => {
        console.log(error);
    })
}

export const navigateToProfileServices = ( first_name, last_name,dispatch, navigation,gender,age_group) =>{
    
    if( first_name && last_name && first_name.length > 0 && last_name.length > 0 && gender && age_group){
        navigation.navigate('PatientServices');
    }else{
        let errors = {}
        if(!first_name){
            errors['first_name'] = ['Kindly fill the first name']
        }else if(!last_name) {
            errors['last_name'] = ['Kindly fill the last name'];
        }else if(!gender){
            alert('Please Select Your Gender');
        }else if (!age_group){
            alert('Please Select Your Age Group');
        }
        dispatch({
            type: ERROR_UPDATE_PROFILE,
            payload: errors
        });
    }    
};
export const navigateToContactList = (navigation) =>{
    navigation.navigate('ContactList');
};

export const navigateToPatientTab = (navigation) =>{
    navigation.navigate('PTabNavigator');
};

// Family member
export const updateFamilyMemberProfile = (first_name, last_name, profile_image, dispatch, navigation) =>{
    if( first_name && last_name && first_name.length > 0 && last_name.length > 0){
        dispatch({
            type: START_UPDATE_PROFILE
        });
        API.updateFamilyMemberProfile(first_name, last_name, profile_image)
        .then((res) => res.json())
        .then(jsonRes =>{
            if(jsonRes.status){
                dispatch({
                    type: OTP_SUCCESS,
                    payload: jsonRes.user
                })
                AsyncStorage.setItem('user', JSON.stringify(jsonRes.user))
                .then(() =>{
                    navigateToFamilyDetails(navigation);
                });
            }else{
                dispatch({
                    type: ERROR_UPDATE_PROFILE,
                    payload: jsonRes.errors
                })
            }
        })
        .catch(err => {
            console.log(err);
        })
    }else{
        let errors = {}
        if(!first_name){
            errors['first_name'] = ['Kindly fill the first name']
        }else if(!last_name) {
            errors['last_name'] = ['Kindly fill the last name'];
        }
        dispatch({
            type: ERROR_UPDATE_PROFILE,
            payload: errors
        });
    }
}

export const fetchFamilyMemberList = (dispatch) =>{
    API.fetchFamilyMemberList()
    .then((res) => res.json())
    .then(jsonRes =>{
        console.log('Response', JSON.stringify(jsonRes))
         if(jsonRes.status){
            dispatch({
                type: FETCH_FAMILY_MEMBER_SUCCESS,
                payload: jsonRes.user
            })
        }else{
            dispatch({
                type: FETCH_FAMILY_MEMBER_FAILED,
                payload: jsonRes.message
            })
        }
    })
    .catch(err => {
        console.log(err);
    })
}

export const fetchFamilyPatientList = (dispatch) =>{
    API.fetchFamilyPatientList()
    .then((res) => res.json())
    .then(jsonRes =>{
        console.log(jsonRes)
         if(jsonRes.status){
            dispatch({
                type: FETCH_FAMILY_PATIENT_SUCCESS,
                payload: jsonRes.user
            })
        }else{
            dispatch({
                type: FETCH_FAMILY_PATIENT_FAILED,
                payload: jsonRes.message
            })
        }
    })
    .catch(err => {
        console.log(err);
    })
}
export const syncContacts = (contacts, dispatch) =>{
    dispatch({
        type: SYNC_CONTACT_START
    })
    API.syncContacts(contacts)
    .then((res) => res.json())
    .then(jsonRes => {
        console.log(jsonRes)
        if(jsonRes.status){
            dispatch({
                type: SYNC_CONTACT_SUCCESS,
                payload: jsonRes.phoneNumbers
            })
        }else{
            dispatch({
                type: SYNC_CONTACT_FAILED
            })
            alert('error to sync contact')
        }
    })
}


export const navigateToFamilyMemberTab = (navigation) =>{
    navigation.navigate('FMTabNavigator');
};
export const navigateToFamilyDetails = (navigation) =>{
    navigation.navigate('FamilyDetails');
};

// Service Provider
export const navigateToServiceProviderDetails = (first_name, last_name, dispatch, navigation) =>{
    if( first_name && last_name && first_name.length > 0 && last_name.length > 0){
        navigation.navigate('ServiceProviderDetails');
    }else{
        let errors = {}
        if(!first_name){
            errors['first_name'] = ['Kindly fill the first name']
        }else if(!last_name) {
            errors['last_name'] = ['Kindly fill the last name'];
        }
        dispatch({
            type: ERROR_UPDATE_PROFILE,
            payload: errors
        });
    }    
};
export const updateServiceProviderProfile = ( isEdit, profile_image, first_name, last_name, gender, age_group, service, qualification, license_number, license_image, additional_certificates, dispatch, navigation) =>{
    dispatch({
        type: START_UPDATE_PROFILE
    });
    API.updateServiceProviderProfile(first_name, last_name, gender, age_group, service, profile_image, qualification, license_number, license_image, additional_certificates)
    .then((res) => res.json())
    .then(jsonRes => {
        console.log("update profile response",jsonRes)
        if(jsonRes.status){
            dispatch({
                type: OTP_SUCCESS,
                payload: jsonRes.user
            })
            AsyncStorage.setItem('user', JSON.stringify(jsonRes.user))
            .then(() =>{
                if(isEdit){
                    navigation.popToTop()
                }else{
                navigateToSPTab(navigation);
                }
            });
        }else{
            dispatch({
                type: ERROR_UPDATE_PROFILE,
                payload: jsonRes.errors
            })
        }
    })
    .catch(err => {
        console.log(error);
    })
}

export const navigateToSPTab = (navigation) =>{
    navigation.navigate('SpTabNavigator');
};

export const navigateToPayment = (color, navigation) =>{
    navigation.navigate('Payment',{color:color});
};
export const saveTokenServ =(fcm, device_Type)=>{
        console.log("*********************************",fcm, device_Type);

    API.saveToken(fcm, device_Type).then(res=>res.json().then(jres=>{
        console.log("*********************************",jres,res);
        // AsyncStorage.setItem('user', JSON.stringify(jres.user)).then(()=>{
            //console.log()
            // dispatch({
            //     type: OTP_SUCCESS,
            //     payload: jres.user
            // });
        // })
    }))
}