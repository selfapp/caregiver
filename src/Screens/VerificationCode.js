import React, { Component } from "react";
import {
  Dimensions,
  Image,
  KeyboardAvoidingView,
  BackHandler,
  View,
  ImageBackground,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,Platform,AsyncStorage
} from "react-native";
import CodeInput from 'react-native-confirmation-code-input';
import { connect } from 'react-redux';
import Loader from './../Components/Loader';
import { verifyOTP } from "../store/actions/user";
import API from '../Components/API';
import {
   BoldText,
   LightText
  } from './../Components/styledTexts';
import colors from "../styles/colors";
import {Button} from '../Components/button';
import { SafeAreaView } from "react-navigation";
let fcmToken='';
let deviceType = false;
const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

class VerificationCode extends Component {
  constructor() {
    super();
    this.state = {
        loader:false,
        otp:'',
        latitude: '',
        longitude: '',
    };
  }
  
  handleClick = () => {
     
  };
  backPressed = () => {
    this.props.navigation.goBack();
    return true;
  };
  
async componentDidMount(){
deviceType = !(Platform.OS === "ios")
fcmToken = await AsyncStorage.getItem('fcmToken');
console.log("--------------------------------------",deviceType,fcmToken);
// API.saveToken(deviceType,fcmToken).then(res=>res.json().then(jres=>{
//     console.log(jres,res);
// }))

}
  componentWillMount() {
    navigator.geolocation.getCurrentPosition(
        (position) => {
            
            console.log("latitude is ", position.coords.latitude)
            console.log("longitude is ", position.coords.longitude)
            this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            });
        },
        (error) => {
            // alert(error.message)
            this.setState({error: error.message})
    },
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000},
    );
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

    reSendOTP() {
        try{
            API.reSendOTP(this.props.navigation.state.params.country_code, this.props.navigation.state.params.phoneNumber)
            .then(res => res.json())
            .then(jsonRes =>{
                    alert(jsonRes.message);
            });
        }catch(error){
            alert(error)
        }
    }

  render(){
    return(
        <View style = {{ flex: 1}}>
           <ImageBackground
              source={require("../../src/assets/bg-img.png")}
              resizeMode="cover"
              style={{ flex: 1,backgroundColor:colors.white }}
            >
                <SafeAreaView style={{ flex: 1 }}>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <KeyboardAvoidingView contentContainerStyle={{flex: 1}} 
                        style={{height: height, width: width}}
                        behavior='position' enabled 
                         keyboardVerticalOffset={-250}
                         >
                        <View style={{flex: 1, marginHorizontal: 30, paddingVertical: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <TouchableOpacity
                                    style={{flex: 0.5}}
                                    onPress={() => {
                                        this.props.navigation.goBack()
                                    }}
                                >
                                    <Image style={{height: 30, width: 30, tintColor: colors.patient}}
                                           resizeMode='contain'
                                           source={require('./../assets/arrow-back.png')}
                                    />
                                </TouchableOpacity>
                                
                                <View style={{flex: 0.5}}/>
                            </View>
                            <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
                                <View style={{ flexDirection:'row' }}>
                                 <Image
                                    source={require("../../src/assets/logo-icon.png")}
                                    style={{ alignItems:'center'}}
                                    />
                                 </View>
                                 
                            </View>
                            <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
                            <BoldText style={{color: 'rgb(119,120,122)', fontSize: 20}}>Verification</BoldText>
                            <BoldText style={{ marginTop: 5, textAlign: 'center', color: 'rgb(119,120,122)', fontSize: 16 }}> {'We have sent a 4 digit verification code to your register mobile number ' + this.props.navigation.state.params.country_code + ' ' + this.props.navigation.state.params.phoneNumber + '. Please enter the verification code below to continue.'} </BoldText>
                                </View>
                            <View style={{flex: 0.7, alignItems: 'center', justifyContent: 'center'}}>
                                <LightText style={{ color: 'red', textAlign: 'center', marginTop: 10 }}>{this.props.errors.otp ? this.props.errors.otp[0] : null}</LightText>
                                
                                <CodeInput
                                    ref="codeInputRef1"
                                    secureTextEntry
                                    returnKeyType='done'
                                    keyboardType='numeric'
                                    codeLength={4}
                                    activeColor={colors.darkBlue}
                                    inactiveColor='rgba(119, 120, 122, 1)'
                                    className={'border-b'}
                                    space={5}
                                    size={40}
                                    inputPosition='left'
                                    onFulfill={(code) => {
                                        this.setState({ otp: code });
                                    }}
                                />
                            </View>

                            <Button
                                value={'Login'}
                                color={colors.colorSignin}
                                onPress={() => this.props.verifyOTP(this.state.otp, 
                                    this.props.navigation.state.params.country_code,
                                     this.props.navigation.state.params.phoneNumber, 
                                     this.state.latitude, this.state.longitude,
                                     fcmToken,deviceType,
                                     this.props.navigation)} 
                                />

                            <View style={{flex: 1, justifyContent: 'center'}}>
                                <View style={{ alignItems:'center'}}>
                                    <TouchableOpacity style={{borderBottomColor: colors.darkBlue, borderBottomWidth:1 }}
                                       onPress = {() => this.reSendOTP()}
                                     >
                                        <BoldText style={{
                                            color: colors.darkBlue,
                                            letterSpacing: 1,
                                            textAlign: 'center',
                                            fontSize:16
                                        }}>Resend Verification code</BoldText>
                                    </TouchableOpacity>
                                    </View>
                            </View>
                        </View>
                        <Loader loading={this.props.loading} />
                    </KeyboardAvoidingView>
                    </TouchableWithoutFeedback>
                    </SafeAreaView>
                </ImageBackground>
        </View>
    )
    }
}

const mapStateToProps = state =>{
    return {
        loading: state.user.loading,
        errors: state.user.errors
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        verifyOTP:(code, country_code, phone_number, latitude, longitude, fcm, device_Type, navigation) => verifyOTP(code, country_code, phone_number, latitude, longitude, fcm, device_Type, dispatch, navigation)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VerificationCode);