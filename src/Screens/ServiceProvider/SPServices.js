import React, {Component} from 'react';
import{
View,
SafeAreaView,
FlatList,
Image
}from 'react-native';
import { connect } from 'react-redux';
import Header from '../../Components/Header';
import { BoldText, LightText } from '../../Components/styledTexts';
import colors from '../../styles/colors';

class SPServices extends Component {
 constructor(props){
     super(props)
 }

 renderItem = ({ item}) =>{
    return(
        <View style={{ flex: 1,  backgroundColor: '#fff', marginHorizontal: 20 }}>
            <View style={{ flex: 1, flexDirection: 'row', marginVertical: 5}}>
                <View style={{ flex: 2}}>
                    <BoldText style={{ color: colors.serviceProvider, fontSize: 16 }}>{item.name}</BoldText>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                    <Image
                    source={require("../../assets/calendar-simple-line-icons.png")}
                    style={{ tintColor: colors.serviceProvider, height: 15, width: 15 }}
                    resizeMode={'contain'}
                    />
                    <LightText style={{ marginLeft: 5}}>10, Jun 2019</LightText>
                </View>
            </View>
            <View style={{ flex: 2, marginVertical: 5}}>
            <LightText>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </LightText>
            </View>
        </View>
    );
}

empetyList =() =>{
    return (
    <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
    <BoldText style={{ color: "red", justifyContent: "center", fontSize: 20 }}>
        {" "}
        No Items Found
    </BoldText>
    </View>
    );
}
renderSeparater = () => {
    return(
        <View style={{ flex: 1, height: 1, backgroundColor: colors.gray05 }}></View>
    )
}


 render(){
     return(
        <View style={{ flex: 1}}>
            <SafeAreaView style={{ flex: 1}}>
                <View style={{flex: .08, backgroundColor:'#fff'}}>
                 <Header
                  leftNavigation={this.props.navigation}
                  color={colors.serviceProvider}
                  value={'Services'}
                />
            </View>
            <View style={{ flex: 1}}>
                        <FlatList
                         data = {data}
                         keyExtractor = {(item, index) => index.toString()}
                         style={{ flex: 1 }}
                         renderItem = {this.renderItem}
                         ItemSeparatorComponent = {this.renderSeparater}
                         ListEmptyComponent = { this.empetyList}
                        />
                    </View>
            </SafeAreaView>
         </View>
     )
 }
}

const data = [
    {
        name:'Mark Todd',
        type:'Trainer'
    },
    {
        name:'John',
        type:'Trainer'
    },
    {
        name:'Antonin',
        type:'Trainer'
    },
    {
        name:'Hafer',
        type:'Trainer'
    },
    {
        name:'Joe',
        type:'Trainer'
    },
    {
        name:'abc',
        type:'Trainer'
    },
    {
        name:'Mark',
        type:'Trainer'
    },
    {
        name:'Antonin',
        type:'Trainer'
    },
    {
        name:'Hafer',
        type:'Trainer'
    },
    {
        name:'John',
        type:'Trainer'
    },
    {
        name:'Joe',
        type:'Trainer'
    },
    {
        name:'Antonin',
        type:'Trainer'
    },
];

const mapStateToProps = state =>{
    return{}
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(SPServices);