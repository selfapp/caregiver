import React, { Component} from 'react';
import {
 View,
 TextInput,
 ScrollView,
 SafeAreaView,
 Image,
 TouchableOpacity,
 FlatList
} from 'react-native';
import { connect } from 'react-redux';
import { 
    navigateToSPTab,
    selectQualification,
    typeLicenseNumber,
    selectLicenseImage,
    selectAdditionalCertificate,
    updateServiceProviderProfile
} from '../../store/actions/user';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet'
import {LightText, BoldText} from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import {Button, OptionButton} from '../../Components/button';


class ServiceProviderDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            title:'',
            options:[],
            cancelIndex:0,
            actionType:0,
            additionalCetificates:[{name:''}, {name:''}, {name:'add'}]
        } 
    }
   

    selectedQualification(value){
        switch(value){
            case 1:
            return 'Post Graduation'
            case 2:
                return 'Graduation'
            case 3:
                return '12'
            case 4: 
                return '10'
            default:
                return 'Please select';
        }
    }

    openGalleryForDocuments(buttonIndex) {
    
        if(buttonIndex === 1) {
            ImagePicker.openPicker({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    if(this.state.actionType === 2){
                        this.props.selectLicenseImage(`data:${image.mime};base64,${image.data}`)
                    }else{
                        let obj = {file:`data:${image.mime};base64,${image.data}`}
                        this.props.selectAdditionalCertificate(obj)
                    }
                });
        } else if(buttonIndex === 0) {
            ImagePicker.openCamera({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    if(this.state.actionType === 2){
                        this.props.selectLicenseImage(`data:${image.mime};base64,${image.data}`)
                    }else{
                        let obj = {file:`data:${image.mime};base64,${image.data}`}
                        this.props.selectAdditionalCertificate(obj)
                    }
                });
        }
    }
    selectedIndex(index){
        if(this.state.actionType === 1){
            this.props.selectQualification(index + 1);
        }else{
            this.openGalleryForDocuments(index)
        }
    }
    showActionSheet = (type) => {
        if(type === 1){
            this.setState({title:'Select your Qualification',options:['Post Graduation','Graduation','12','10','Cancel'], cancelIndex:4, actionType:type},()=>{
                this.ActionSheet.show()
            })
        }else{
            this.setState({title:'Which one do you like ?',options:['Take photo', 'Choose from Library', 'Cancel'], cancelIndex:2, actionType:type},()=>{
                this.ActionSheet.show()
            })
        }

      }


    render(){
        return(
            <View style={{ flex: 1}}>
                <SafeAreaView style={{ flex: 1}}>
                <View style={{flex: .08, backgroundColor:'#fff'}}>
                        <Header
                        leftNavigation={this.props.navigation}
                        color={colors.serviceProvider}
                        value={'Profile'}
                        />
                    </View>
                    
                    <ScrollView style={{ flex: 1}}>
                    <View style={{ flex: 1, marginHorizontal: 10 }}>
  {/* Qualification          */}
                        <View style={{ flex: 1}}>
                            <View style={{  flexDirection: 'row', alignItems:'center', marginVertical: 20}}>
                                    <Image
                                        style={{ height: 15, width: 15, borderRadius: 15/2, tintColor: colors.serviceProvider }}
                                        source={require("../../assets/qualification.png")}
                                    />
                                    <BoldText style={{ marginLeft: 10, color: colors.serviceProvider, fontSize: 14}}>Qualification</BoldText>
                            </View>
                            <TouchableOpacity
                                onPress={() => this.showActionSheet(1)}
                            >
                            <View style={{ marginHorizontal: 20 , flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                                
                                <LightText>{this.selectedQualification(this.props.user.qualification)}</LightText>
                                <Image
                                source = { require('../../assets/down-arrow.png')}
                                />
                            </View>
                            </TouchableOpacity>

  {/* Actionsheet For Option */}
                    <ActionSheet
                        ref={o => this.ActionSheet = o}
                        title={this.state.title}
                        options={this.state.options}
                        cancelButtonIndex={this.state.cancelIndex}
                        onPress={(index) => { this.selectedIndex(index) }}
                        />
                            <View style={{marginTop: 5, height: 1, backgroundColor: colors.gray05, marginHorizontal: 15}}/>
                        </View>
{/* License */}
                        <View style={{ flex: 1}}>
                            <View style={{  flexDirection: 'row', alignItems:'center', marginVertical: 20}}>
                                    <Image
                                        style={{ height: 15, width: 15, tintColor: colors.serviceProvider }}
                                        source={require("../../assets/certificate.png")}
                                        resizeMode={'contain'}
                                    />
                                    <BoldText style={{ marginLeft: 10, color: colors.serviceProvider, fontSize: 14}}>License</BoldText>
                            </View>
                            <View style={{ marginHorizontal: 20 , flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                                <TextInput
                                    style={{fontSize:16, alignItems:"flex-start", color:colors.serviceProvider, marginLeft: 10}} 
                                    placeholder ="ID NUMBER"
                                    value={this.props.user.license_number}
                                    onChangeText={(license_number) => this.props.typeLicenseNumber(license_number)}
                                />
                            </View>
                            <View style={{marginTop: 5, height: 1, backgroundColor: colors.gray05, marginHorizontal: 15}}/>
                         </View>
{/* Photo ID */}
                         <View style={{ flex: 1}}>
                            <View style={{ justifyContent: 'center', marginVertical: 20, marginHorizontal: 15 }}>
                                <TouchableOpacity style={{ flex: 1, padding: 8, borderStyle: 'dotted', borderWidth: 1, borderRadius: 1, flexDirection: 'row', borderColor:colors.serviceProvider, alignItems:'center'}}
                                 onPress={() => this.showActionSheet(2)}
                                >
                                    <Image
                                            style={{ flex: 1, height: 15, width: 15, tintColor: colors.serviceProvider }}
                                            source={require("../../assets/photo-camera.png")}
                                            resizeMode={'contain'}
                                    />
                                    <LightText style={{ color: colors.serviceProvider, fontSize: 16, flex: 4}}> Take photo of ID</LightText>
                                </TouchableOpacity>
                            </View>
                            <View style={{ justifyContent: 'center', marginHorizontal: 15, flexDirection:'row' }}>
                                
                                <Image
                                    style={{ flex: 1,height: 40 }}
                                    source={(this.props.user.license_image) ? ({uri: this.props.user.license_image}) : require("../../assets/certificate.png")}
                                    resizeMode={'contain'}
                                    />
                                    <View style={{ flex: 4, padding: 10}}>
                                        <LightText>Driver's License </LightText>
                                        <View style={{marginTop: 5, height: 1, backgroundColor: colors.serviceProvider}}/>
                                    </View>
                            </View>
                            <View style={{marginTop: 15, height: 1, backgroundColor: colors.gray05, marginHorizontal: 15}}/>
                        </View>
{/* Additional certificates*/}
                            <View style={{ flex: 1 }}>
                                <View style={{  flexDirection: 'row', alignItems:'center', marginVertical: 20}}>
                                        <Image
                                            style={{ height: 25, width: 25, tintColor: colors.serviceProvider }}
                                            source={require("../../assets/certificate.png")}
                                            resizeMode={'contain'}
                                        />
                                        <BoldText style={{ marginLeft: 10, color: colors.serviceProvider, fontSize: 14}}>Additional Certificates</BoldText>
                                </View>
                                <View style={{ flex: 1}}>
                                    <FlatList
                                    style={{ marginTop: 20, marginHorizontal:20 }}
                                    data={this.props.user.additionalCertificates}
                                    horizontal={true}
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({item}) =>(
                                        <View style={{ padding: 5, alignItems:'center', justifyContent:'center' }}>
                                            {item === 'add' ? (
                                                <TouchableOpacity
                                                style={{ alignItems:'center'}}
                                                onPress={() => this.showActionSheet(3)}
                                                >
                                                    <Image
                                                        source={require('../../assets/photo-camera-add.png')}
                                                        style={{ height: 50, width: 50}}
                                                        resizeMode={'contain'}
                                                    />
                                                </TouchableOpacity>
                                            ) : ( 
                                                <Image
                                                    source={{uri: item.file}}
                                                    style={{ height: 50, width: 50}}
                                                    resizeMode={'contain'}
                                                />
                                                )}
                                    </View>
                                    )}
                                    />
                                </View>
                                <View style={{marginTop: 15, height: 1, backgroundColor: colors.gray05, marginHorizontal: 15}}/>
                            </View>
{/* Bottom Button       */}
                            <View style={{ flex: 1 }}>
                             <Button
                                value={'Continue'}
                                color={colors.serviceProvider}
                                onPress={() => {
                                        let addionalData = this.props.user.additionalCertificates
                                       if(this.props.user.additionalCertificates.length){
                                        addionalData.pop();
                                       }                  
                                    this.props.updateServiceProviderProfile(this.props.isEdit, this.props.user.profile_image, this.props.user.first_name, this.props.user.last_name, this.props.user.gender, this.props.user.age_group, this.props.user.service, this.props.user.qualification, this.props.user.license_number, this.props.user.license_image, addionalData, this.props.navigation)
                                    addionalData.push('add');
                                }
                                }
                                />
                            </View>
                    </View>
                    
                </ScrollView>
                </SafeAreaView>
            </View>
        )
    }
}

const mapStateToProps = state =>{
    return{
        user: state.user,
        isEdit: state.user.isEdit
    }
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
        selectQualification: (qualification_id) => {
            dispatch(selectQualification(qualification_id));
          },
          typeLicenseNumber:(license_number) =>{
              dispatch(typeLicenseNumber(license_number))
          },
          selectLicenseImage:(license_image)=>{
            dispatch(selectLicenseImage(license_image))
          },
          selectAdditionalCertificate:(certificate) =>{
            dispatch(selectAdditionalCertificate(certificate))
          },
          updateServiceProviderProfile:(isEdit, profile_image, first_name, last_name, gender, age_group, service, qualification, license_number, license_image, additional_certificates, navigation) => updateServiceProviderProfile(isEdit, profile_image, first_name, last_name, gender, age_group, service, qualification, license_number, license_image, additional_certificates, dispatch, navigation),
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(ServiceProviderDetails);