import React, {Component} from 'react';
import{
View,
SafeAreaView,
Image,
Dimensions,
TouchableOpacity,
ScrollView,
Switch
}from 'react-native';
import { connect } from 'react-redux';
import {dutyOnOff} from '../../store/actions/Services';
import { logout } from '../../store/actions/user';
import Header from '../../Components/Header';
import colors from '../../styles/colors';
import { LightText} from '../../Components/styledTexts';
import Loader from '../../Components/Loader';
import API from '../../Components/API';
import { EDIT_PROFILE } from '../../store/ActionTypes';


const {height, width} = Dimensions.get('window');

class SPProfile extends Component {
 constructor(props){
     super(props)
     this.state = {
        latitude: 28.515038,
        longitude: 77.080476,        
     }
 }
 componentDidMount() {       
    navigator.geolocation.getCurrentPosition(
        (position) => {
            this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            });
        },
        (error) => {
            // alert(error.message)
    },
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000},
    );
  }
  

  editProfile(){
    console.log('is edit value', this.props.isEdit)
    this.props.editProfile()
    this.props.navigation.navigate('ServiceProviderProfile')
}

 render(){
     return(
         <View style={{ flex: 1}}>
            <SafeAreaView style={{ flex: 1}}>
                <View style={{flex: .08, backgroundColor:'#fff'}}>
                        <Header
                        color={colors.serviceProvider}
                        value={'Profile'}
                        onPressRight={()=>this.editProfile()}
                        rightIcon={require('../../assets/edit.png')}
                        />
                </View>
                <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
{/* Profile Pic */}
                    <View style={{ justifyContent:'center', alignItems:'center',marginVertical: 30 }}>
                        <View style={{ height: width/3, width: width/3, borderRadius: (width/3)/ 2, borderWidth: 2, borderColor: colors.serviceProvider, justifyContent:'center', alignItems:'center' }}>
                        <Image
                            style={{ height: width/3 - 10, width: width/3 - 10, borderRadius: (width/3 - 10)/ 2 }}
                            source={(this.props.user.profile_image) ? ({uri: `${API.baseURL}${this.props.user.profile_image}` }) : require("../../assets/profile.png")}
                        />
                        </View>
                        <LightText style={{ color: colors.serviceProvider, marginVertical: 10 }}>{this.props.user.first_name + ' ' + this.props.user.last_name}</LightText>
                        <LightText style={{ fontSize: 12}}>Service Provider</LightText>           
                    </View>
                    <View style={{ marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
                    <View style={{ flex:5, marginHorizontal: 10}}>
                    <View style={{ flex: 2, marginVertical: 10, marginHorizontal:10 }}>
                        <View style={{ flex: 1, flexDirection: 'row', marginVertical: 10, marginHorizontal: 10}}>
                             <Image
                                style={{height:15,width:15, tintColor: colors.gray04}}
                                source={require("../../assets/phone.png")}
                                />
                                <LightText style={{ marginLeft: 15, height: 30}}>{this.props.user.phone_number}</LightText>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', marginVertical: 10, marginHorizontal: 10}}>
                             <Image
                                style={{height:15,width:15, tintColor: colors.gray04}}
                                source={require("../../assets/gender.png")}
                                />
                                <LightText style={{ marginLeft: 15, height: 30}}>{this.props.user.gender === 1 ? 'Male' : 'Female'}</LightText>
                        </View>
                        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
                    </View>

                    <View style={{ flex: 1, marginVertical: 10, marginHorizontal:10, justifyContent:'center'}}>
                        <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 10, marginHorizontal: 10}}
                            onPress={()=> this.props.navigation.navigate('SPServices')}
                        >
                             <Image
                                style={{height:15,width:15, tintColor: colors.gray04}}
                                source={require("../../assets/my-visit.png")}
                                />
                                <LightText style={{ marginLeft: 15}}>My Visit</LightText>
                        </TouchableOpacity>
                        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
                    </View>

                    <View style={{ flex: 1, marginVertical: 10, marginHorizontal:10, justifyContent:'center'}}>
                        <View style={{ flexDirection: 'row', marginVertical: 10, marginHorizontal: 10}}>
                             <Image
                                style={{height:15,width:15, tintColor: colors.gray04}}
                                source={require("../../assets/money.png")}
                                />
                                <LightText style={{ marginLeft: 15}}>Payment</LightText>
                        </View>
                        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
                    </View>
                    
                    <View style={{ flex: 1, marginTop: 10, marginHorizontal:10}}>
                        <View style={{ flexDirection: 'row', marginVertical: 10, marginHorizontal: 10}}>
                             <Image
                                style={{height:15,width:15, tintColor: colors.gray04}}
                                source={require("../../assets/settings.png")}
                                />
                                <LightText style={{ marginLeft: 15}}>Settings</LightText>
                        </View>
                        
                        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
                    </View>
    {/* Dutty on                 */}
                    <View style={{ flex: 1, marginVertical: 10, marginHorizontal:10, justifyContent:'center'}}>
                        <View style={{ flexDirection: 'row', marginVertical: 10, marginHorizontal: 10, justifyContent:'space-between', alignItems:'center'}}>
                             <View style={{ flexDirection:'row'}}>
                             <Image
                                style={{height:15,width:15, tintColor: colors.gray04}}
                                source={require("../../assets/member-icon.png")}
                                resizeMode={'contain'}
                                />
                                <LightText style={{ marginLeft: 15}}>Duty</LightText>
                                </View>
                                <Switch 
                                    trackColor={'rgb(178,210,204)'} 
                                    thumbColor={colors.serviceProvider}
                                    value={this.props.service.available}  
                                    onValueChange ={(switchValue)=>
                                        this.props.dutyOnOff(this.state.latitude, this.state.longitude, switchValue)
                                    }
                                />  
                        </View>
                    </View>
                    </View>

                    <TouchableOpacity style={{ backgroundColor: colors.serviceProvider}}
                        onPress={() => this.props.logout(this.props.navigation)}
                    >
                        <View style={{ flexDirection: 'row', marginVertical: 15, marginHorizontal: 30}}>
                             <Image
                                style={{height:15,width:15, tintColor: '#fff'}}
                                source={require("../../assets/power-button.png")}
                                />
                                <LightText style={{ marginLeft: 15, color:'#fff'}}>Log out</LightText>
                        </View>
                    </TouchableOpacity>
                </View>

                </ScrollView>
                <Loader loading={this.props.service.loading} />
             </SafeAreaView>
         </View>
     )
 }
}

const mapStateToProps = state =>{
    return{
        user: state.user,
        service: state.services,
        isEdit: state.user.isEdit 
    }
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
        dutyOnOff:(lat, lng, value) => dutyOnOff(lat, lng, value, dispatch),
        logout:(navigation) => logout(dispatch, navigation),
        editProfile:() =>{
            dispatch({
                type: EDIT_PROFILE,
            });
           },
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(SPProfile);