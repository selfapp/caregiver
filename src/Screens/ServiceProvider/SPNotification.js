import React, {Component} from 'react';
import{
View,
SafeAreaView,
SectionList,
Image,
Text,
TouchableOpacity
}from 'react-native';
import { connect } from 'react-redux';

import { 
    fetchAppointmentList,
    acceptRejectAppointment
 } from '../../store/actions/Services';
import Header from '../../Components/Header';
import { BoldText, LightText } from '../../Components/styledTexts';
import colors from '../../styles/colors';
import General from '../../styles/General';
import Moment from 'moment';

class SPNotification extends Component {
 constructor(props){
     super(props)
 }
 componentDidMount(){
    this.props.fetchAppointmentList();
    console.log('=============did mount', this.props.services.appointmentList)
    let timerId = setInterval(() => this.props.fetchAppointmentList(), 3000);
 }
renderHeader=(title)=>{
    console.log("-------header title ----",title)
    return(
        <View>
            <View style={{ flex: 1, height: 1, backgroundColor: colors.gray05 }}></View>
            <View style={{height:50,width:'100%',paddingHorizontal:10,paddingVertical:5,justifyContent:'center'}}>
            <BoldText style={{ color: colors.serviceProvider, fontSize: 14}}>{title}</BoldText>
            </View>
            <View style={{ flex: 1, height: 1, backgroundColor: colors.gray05 }}></View>
        </View>
        
      )
}
 renderItem = ({ item}) =>{
     if(item.status === "pending"){
        return(
            <View style={{padding:15}}>
                <View style={[General.Card,{padding:10}]}>
<View style={{ flex: 1, flexDirection: 'row', marginVertical: 5}}>
                <View style={{ flex: 2}}>
                    <BoldText style={{ color: colors.serviceProvider, fontSize: 14}}>{item.first_name} has requested for {item.service_provided}</BoldText>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                    <Image
                    source={require("../../assets/calendar-simple-line-icons.png")}
                    style={{ tintColor: colors.serviceProvider, height: 15, width: 15, paddingRight:5 }}
                    resizeMode={'contain'}
                    />
                    <LightText style={{ marginLeft: 5}}>{item.date}</LightText>
                </View>
            </View>
            <View style={{ flex: 1, flexDirection: 'row', marginVertical: 5}}>
                <View style={{ flex: 2,flexDirection:'row'}}>
                <Text>Service Provided:</Text> 
                <LightText style={{ marginLeft: 5}}>{item.service_provided}</LightText>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                </View>
            </View>

                <View style={{ flex: 1, height: 1, backgroundColor: colors.gray05 , marginBottom:10}}></View>
                {(item.status === 'pending')?(
                <View style={{width:'100%', flexDirection:'row', justifyContent:'space-around'}}>
                <TouchableOpacity style={{backgroundColor:colors.lightBlack, alignItems:'center', padding:6, paddingHorizontal:30, height:30, borderRadius:15}}
                 onPress={()=> this.props.acceptRejectAppointment(item.id, 2,this.props.userData.phone_number, this.props.navigation)}

                >
                        <LightText style={{ color: "#fff"}}>{`Reject`}</LightText>
                </TouchableOpacity>
                <TouchableOpacity style={{backgroundColor:colors.green03, alignItems:'center', padding:6, paddingHorizontal:30, height:30, borderRadius:15}}
                   onPress={()=> {
                       this.props.acceptRejectAppointment(item.id, 1,this.props.userData.phone_number, this.props.navigation);
                    }}
                // onPress={()=> this.props.navigation.navigate('tripDetails')}
                >
                        <LightText style={{ color: "#fff"}}>{`Accept`}</LightText>
                </TouchableOpacity>
            </View>
           ):(null)}
                </View>
          </View>
        );
     }
     else{
    return(
       
        <View style={{ flex: 1,  backgroundColor: '#fff', marginHorizontal: 20, marginVertical: 10 }}>
            <View style={{ flex: 1, flexDirection: 'row', marginVertical: 5}}>
                <View style={{ flex: 2}}>
                    <BoldText style={{ color: colors.serviceProvider, fontSize: 14}}>{item.title}</BoldText>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                    <Image
                    source={require("../../assets/calendar-simple-line-icons.png")}
                    style={{ tintColor: colors.serviceProvider, height: 15, width: 15, paddingRight:5 }}
                    resizeMode={'contain'}
                    />
                    <LightText style={{ marginLeft: 5}}>{item.date}</LightText>
                </View>
            </View>
            <View style={{ flex: 2, marginVertical: 5}}>
            <LightText>Service provided : { item.service_provided}. </LightText>
            </View>
            {(item.status === 'pending')?(
                <View style={{width:'100%', flexDirection:'row', justifyContent:'space-around'}}>
                <TouchableOpacity style={{backgroundColor:colors.lightBlack, alignItems:'center', padding:6, paddingHorizontal:30, height:30, borderRadius:15}}
                 onPress={()=> this.props.acceptRejectAppointment(item.id, 2,this.props.userData.phone_number, this.props.navigation)}
                >
                        <LightText style={{ color: "#fff"}}>{`Reject`}</LightText>
                </TouchableOpacity>
                <TouchableOpacity style={{backgroundColor:colors.familyMember, alignItems:'center', padding:6, paddingHorizontal:30, height:30, borderRadius:15}}
                //    onPress={()=> this.props.acceptRejectAppointment(item.id, 1)}
                onPress={()=> this.props.navigation.navigate('tripDetails')}

                >
                        <LightText style={{ color: "#fff"}}>{`Accept`}</LightText>
                </TouchableOpacity>
            </View>
            ):(null)}
            
        </View>
    );
     }
}

empetyList =() =>{
    return (
    <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
    <BoldText style={{ color: "red", justifyContent: "center", fontSize: 20 }}>
        {" "}
        No Notifications
    </BoldText>
    </View>
    );
}
renderSeparater = () => {
    return(
        <View style={{ flex: 1, height: 1, backgroundColor: colors.gray05 }}></View>
    )
}


 render(){

     return(
        <View style={{ flex: 1}}>
            <SafeAreaView style={{ flex: 1}}>
                <View style={{flex: .08, backgroundColor:'#fff'}}>
                 <Header
                  color={colors.serviceProvider}
                  value={'Notifications'}
                />
            </View>
            <View style={{ flex: 1}}>
            <SectionList
        sections={this.props.services.appointmentList}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderItem}
        ItemSeparatorComponent = {this.renderSeparater}
        renderSectionHeader={({ section }) => (
            section.data.length > 0 ? this.renderHeader(section.title) : (null)
         )}
        // renderSectionHeader={({ section}) => (
        //     section.data.length > 0 ? this.renderHeader(title) : (null)
            
        //   )}
          ListEmptyComponent={this.empetyList}
        // renderSectionHeader={this.renderHeader}
      />
                        {/* <FlatList
                         data = {this.props.services.appointmentList}
                         keyExtractor = {(item, index) => index.toString()}
                         style={{ flex: 1 }}
                         renderItem = {this.renderItem}
                         ItemSeparatorComponent = {this.renderSeparater}
                         ListEmptyComponent = { this.empetyList}
                        /> */}
                    </View>
            </SafeAreaView>
         </View>
     )
 }
}


const mapStateToProps = state =>{
    return{
        services: state.services,
        userData:state.user,
    }
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
        fetchAppointmentList:() => fetchAppointmentList(dispatch),
        acceptRejectAppointment:(appointmentId, status,phone, navigation) => acceptRejectAppointment(appointmentId, status,phone, navigation, dispatch)
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(SPNotification);