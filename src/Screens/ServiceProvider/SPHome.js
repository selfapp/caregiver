import React, {Component} from 'react';
import{
View,
SafeAreaView,
StyleSheet
}from 'react-native';
import { connect } from 'react-redux';
import MapView, {Marker, Circle, PROVIDER_GOOGLE} from 'react-native-maps';
import colors from '../../styles/colors';


class SPHome extends Component {
 constructor(props){
     super(props)
     this.state = {
        latitude: 47.609243,
        longitude: -122.325177,
        error:null
    };
 }
 componentDidMount() {       
    navigator.geolocation.getCurrentPosition(
        (position) => {
            
            this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            });
        },
        (error) => {
            // alert(error.message)
            this.setState({error: error.message})
    },
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000},
    );
  }

 render(){
     return(
        <View style={{ flex: 1}}>
            <SafeAreaView style={{ flex: 1}}>
            <View style={{ flex: 1}}>
                
            <MapView
                style={{flex: 1}}
                provider={PROVIDER_GOOGLE}
                region={{
                    latitude: this.state.latitude,
                    longitude: this.state.longitude,
                    latitudeDelta: 0.0722,
                    longitudeDelta: 0.0221,
                }}
            >
                <Circle center={{latitude: this.state.latitude, longitude: this.state.longitude}} radius={300}
                        fillColor={'rgba(65, 145, 170, 0.26)'} strokeColor={'transparent'}/>
                <Marker coordinate={{latitude: this.state.latitude, longitude: this.state.longitude}}
                        // image={require('../../assets/map-pointer.png')}
                        // style={{backgroundColor: colors.serviceProvider}}
                />
            </MapView>

            </View>
            </SafeAreaView>
         </View>
     )
 }
}


const mapStateToProps = state =>{
    return{}
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(SPHome);