import React, {Component} from 'react';
import{
View,
SafeAreaView,
StyleSheet,
TextInput,
Image,
Dimensions,
FlatList,
TouchableOpacity,
Text,
Alert
}from 'react-native';
import { connect } from 'react-redux';
import { fetchServicesByLocation,  requestForService,
  getAppointmentStatus} from '../../store/actions/Services';
import MapView, {Marker, Circle, PROVIDER_GOOGLE} from 'react-native-maps';
import Header from '../../Components/Header';
import colors from '../../styles/colors';
import { LightText, BoldText } from '../../Components/styledTexts';
import API from '../../Components/API';
import MapViewDirections from 'react-native-maps-directions';
import Loader from './../../Components/Loader';
const origin = {latitude: 37.3318456, longitude: -122.0296002};
const destination = {latitude: 37.771707, longitude: -122.4053769};
const GOOGLE_MAPS_APIKEY = 'AIzaSyBy4SEL5Madg88ynd8ZbqARpsGMeBTT1As';
const {height, width} = Dimensions.get('window');

class PServices extends Component {

 constructor(props){
     super(props)
     this.state = {
      latitude: 28.515038,
      longitude: 77.080476,
      searchText:'',
      showUser:false,
      selectedServiceId:1,
      loading:false,
      // arrServices:[]
  };
 }
 componentDidMount() {       
  navigator.geolocation.getCurrentPosition(
      (position) => {
          this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
          });
          this.props.fetchServicesByLocation(position.coords.latitude, position.coords.longitude)
      },
      (error) => {
          // alert(error.message)
          this.setState({error: error.message})
  },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000},
  );
  // let timerId = setInterval(() => this.getAppointmentStatusDetail(), 3000);
    // this.setState({timer: timerId})
}

componentWillUnmount(){
  clearInterval(this.state.timer)
  // this.clearInterval(this.state.timer);
}
getAppointmentStatusDetail(){
  // console.log("*****************************",this.props.requestAppointment)
  if(this.props.requestAppointment != null){
    console.log("*1",this.props.getAppointmentStatus(this.props.requestAppointment.id))
      this.props.getAppointmentStatus(this.props.requestAppointment.id)
    }else{
      console.log('no appointment')
    }
}


setSearchText = async (event) => {
  //  const _this = this;
  let searchText = event.nativeEvent.text;
  console.log(searchText);
  this.setState({searchText});
  // this.getLoactionDetails()
  
}

showAlert(title, body) {
  Alert.alert(
    title, body,
    [
        { text: 'OK', onPress: () => {this.setState({loading:true }); this.endTRip()}},
        { text: 'Cancel', onPress: () => console.log('OK Pressed') },
    ],
    { cancelable: false },
  );
}
endTRip = ()=>{
 
  API.request("/service-providers/end-trip","PATCH",null).then(res=>res.json()).then((responseJson) => {
    this.setState({ loading:false })
    if(responseJson.status===true){
      alert(responseJson.message)
      this.props.navigation.goBack()
    }
    this.setState({ loading:false })
  })
}

searchMe=()=>{
  console.log(this.state.searchText)
   API.getLoactionDetails(this.state.searchText,async(res)=>{
    if(res.latitude && res.longitude){
     await  this.setState({
        latitude:res.latitude,
        longitude:res.longitude,
      },console.log("-------------------------",this.state.latitude, this.state.longitude, res))
      this.props.fetchServicesByLocation(res.latitude, res.longitude,)
    }
   
    
  console.log(res)
  })
}

_renderItem = ({item, index})=>{
console.log("Item values ")
console.log(item)
  return(
    <View style={{ flex:1, padding:10, justifyContent:'space-around'}}>
    <View style={{ borderRadius:5, backgroundColor:(this.state.selectedServiceId === item.id)? colors.lightBlack : colors.lightGray, padding:3}}>
      <LightText style={{color:'#fff'}}>{item.name}</LightText>
    </View>
    <TouchableOpacity style={{ alignItems:'center', padding:3}}
      onPress={()=> {
        this.setState({selectedServiceId:item.id})
     }}
    >
       <Image
         source={require('../../assets/services/service-a.png')}
         style={{ }}
       />
    </TouchableOpacity>
     <View style={{ alignItems:'center'}}>
      <LightText >{`$ ${item.amount}`}</LightText>
    </View>
  </View>
  )
 
}
empetyList =() =>{
  return (
  <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
  <BoldText style={{ color: "grey", justifyContent: "center", alignItems:'center',  fontSize: 18 }}>
       No services present near this location.
  </BoldText>
  </View>
  );
}

 render(){
     return(
        <View style={{ flex: 1}}>
        <SafeAreaView style={{ flex: 1}}>
        <View style={{flex: .08, backgroundColor:'#fff'}}>
         <Header
          leftNavigation={this.props.navigation}
          color={colors.patient}
          value={'Appoinment'}
        />
        </View>
        <View 
                style={{position:'absolute',zIndex:1,bottom:2, height: 130, width:width-40, marginHorizontal: 20, alignItems:'center', justifyContent:'center', marginTop: 10,borderRadius: 2,
                backgroundColor: '#FFFFFF',
                shadowColor: '#451B2D',
                shadowOffset: {width: 0, height: 5},
                shadowOpacity: 0.1,
                shadowRadius: 21,}}>
<TouchableOpacity style={{backgroundColor:'red', alignItems:'center', padding:6, paddingHorizontal:30, height:40, borderRadius:1,width:'90%',justifyContent:'center'}}
                //    onPress={()=> this.props.acceptRejectAppointment(item.id, 1)}
                onPress={()=> this.showAlert("End Appointment", "Are you sure? click ok to end appointment.")}
                >
                        <LightText style={{ color: "#fff"}}>{`End Appointment`}</LightText>
                </TouchableOpacity>
                <TouchableOpacity style={{backgroundColor:colors.green03,marginTop:20, alignItems:'center', padding:6, paddingHorizontal:30, height:40, borderRadius:1,width:'90%',justifyContent:'center'}}
                //    onPress={()=> this.props.acceptRejectAppointment(item.id, 1)}
                  onPress={()=> this.props.navigation.navigate('ChatScreen',{user:'SP'})}
                >
                        <LightText style={{ color: "#fff"}}>Chat</LightText>
                </TouchableOpacity>
                    </View>
                    
        <View style={{ flex: 1}}>
      
        <MapView
                style={{flex: 1}}
                provider={PROVIDER_GOOGLE}
                region={{
                    latitude: this.state.latitude,
                    longitude: this.state.longitude,
                    latitudeDelta: 0.0722,
                    longitudeDelta: 0.0221,
                }}
            >
                  <MapViewDirections
    origin={origin}
    destination={destination}
    apikey={GOOGLE_MAPS_APIKEY}
  />
                {/* <Circle center={{latitude: this.state.latitude, longitude: this.state.longitude}} radius={300}
                        fillColor={'rgba(65, 145, 170, 0.26)'} strokeColor={'transparent'}/>
                <Marker coordinate={{latitude: this.state.latitude, longitude: this.state.longitude}}
                        // image={require('../../assets/map-pointer.png')}
                        // style={{backgroundColor: colors.patient}}
                /> */}
            </MapView>
</View>

        </SafeAreaView>
        <Loader loading={this.state.loading} />
     </View>
     )
 }
}


const mapStateToProps = state =>{
    return{
      avilableServices: state.services.avilableServices,
      requestAppointment: state.services.requestAppointment
    }
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
      fetchServicesByLocation:(lat, lng ) => fetchServicesByLocation(lat, lng, dispatch),
      requestForService:(serviceId, patientId, lat, lng) => requestForService(serviceId, patientId, lat, lng, dispatch),
      getAppointmentStatus:(id) => getAppointmentStatus(id, dispatch)
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(PServices);