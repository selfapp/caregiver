import React, { Component } from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  TextInput,
  View,
  FlatList,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet'
import { 
  navigateToServiceProviderDetails,
  typeFirstName, 
  typeLastName,
  selectGender,
  selectAgeGroup,
  selectService,
  selectProfileImage
} from '../../store/actions/user';
import {LightText} from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import API from '../../Components/API';

import {Button, OptionButton} from '../../Components/button';

const width = Dimensions.get('window').width;
const servicesData = [
    {
        name:'Eating',
        id:1
    },
    {
        name:'Bathing',
        id:2
    },
    {
        name:'Toileting',
        id:3
    },
    {
        name:'Dressing',
        id:4
    },
    {
        name:'Transferring from chair to Bed or vice versa',
        id:5
    },
    {
      name:'Walking or moving about',
      id:6
  },
]


class ServiceProviderProfile extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  componentWillMount() {
  }

  componentWillUnmount() {
  }

  openGalleryForProfilePic(buttonIndex) {
    
    if(buttonIndex === 1) {
        ImagePicker.openPicker({
            width: 1000,
            height: 1000,
            cropping: true,
            includeBase64: true,
            mediaType: 'photo'
        })
            .then(image => {
              this.props.selectProfileImage(`data:${image.mime};base64,${image.data}`)
            });
    } else if(buttonIndex === 0) {
        ImagePicker.openCamera({
            width: 1000,
            height: 1000,
            cropping: true,
            includeBase64: true,
            mediaType: 'photo'
        })
            .then(image => {
              this.props.selectProfileImage(`data:${image.mime};base64,${image.data}`)
            });
    }
}
  showActionSheet = () => {
    this.ActionSheet.show()
  }

  render(){
    return(

   <View style={{ flex: 1}}>
      <SafeAreaView style={{ flex: 1}}>
        <View style={{flex: .08, backgroundColor:'#fff'}}>
            <Header
              leftNavigation={this.props.navigation}
              color={colors.serviceProvider}
              value={this.props.isEdit ? 'Edit Profile' : 'Profile'}
            />
        </View>

      <ScrollView style={{ flex: 1}}>
        <View style={{marginHorizontal: 10}}>

{/* Profile Details */}
        <View style={{ flexDirection: 'row', alignItems:'center', marginVertical: 20}}>
          <Image
            style={{marginLeft:3, tintColor: colors.serviceProvider}}
            source={require("../../assets/tell-us.png")}
          />
          <LightText style={{ color: colors.serviceProvider, marginLeft: 5}}> Tell us bit about yourself</LightText>
        </View>

{/* Profile Pic */}
        <View style={{ justifyContent:'center', alignItems:'center',marginVertical: 10 }}>
        <View style={{ justifyContent:'flex-start', alignItems:'flex-end'}}>
            <View style={{ height: width/3, width: width/3, borderRadius: (width/3)/ 2, borderWidth: 2, borderColor: colors.serviceProvider, justifyContent:'center', alignItems:'center' }}>
             
            {
               this.props.profile_picture?
               <Image
                style={{ height: width/3 - 10, width: width/3 - 10, borderRadius: (width/3 - 10)/ 2 }}
                source={(this.props.profile_picture[0]== '/') ? ({ uri: `${API.baseURL}${this.props.profile_picture}` }):({ uri: this.props.profile_picture })  }
              />:
              <Image
                style={{ height: width/3 - 10, width: width/3 - 10, borderRadius: (width/3 - 10)/ 2 }}
                source={require("../../assets/profile.png")}
              />
             } 
            </View>
            <View style={{ position:'absolute', borderRadius:20, alignItems:'center', justifyContent:'center', height:40, width:40, backgroundColor:'#fff'}}>
                <TouchableOpacity
                  onPress={() => { this.showActionSheet()}}
                 >
                <Image
                  source={require('../../assets/edit.png')}
                  style={{ height: 25, width: 25, tintColor:colors.serviceProvider}}
                />
                </TouchableOpacity>
              </View>
              </View>
              <LightText style={{ color: colors.serviceProvider, marginVertical: 10 }}>Upload photo</LightText>
              <LightText style={{ fontSize: 12}}>Service Provider</LightText>           
        </View>
        {/* Actionsheet For Option */}
         <ActionSheet
                  ref={o => this.ActionSheet = o}
                  title={'Which one do you like ?'}
                  options={['Take photo', 'Choose from Library', 'Cancel']}
                  cancelButtonIndex={2}
                  onPress={(index) => { this.openGalleryForProfilePic(index) }}
                  />
        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
{/* Text Details */}
        <View style={{ marginVertical: 25 }}>
          <View style={{ marginHorizontal: 15,marginVertical: 10}}>
            <View style={{ flexDirection: 'row', alignItems:'center'}}>
            <Image
              style={{height:15,width:15, tintColor: colors.serviceProvider}}
              source={require("../../assets/name-icon.png")}
            />
              <TextInput
                style={{ flex: 1, height: 30, fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                placeholder ="First name"
                value={this.props.first_name}
                onChangeText={(first_name) => this.props.typeFirstName(first_name)}
              />
              </View>
              <LightText style={{ color: 'red', marginTop: 5 }}>{this.props.errors.first_name ? this.props.errors.first_name : null}</LightText>
              <View style={{ height: 1, backgroundColor: colors.gray05}}/>
          </View>

          <View style={{ marginHorizontal: 15, marginTop: 15}}>
            <View style={{ flexDirection: 'row', alignItems:'center'}}>
            <Image
              style={{height:15,width:15, tintColor: colors.serviceProvider}}
              source={require("../../assets/name-icon.png")}
            />
              <TextInput
                style={{flex: 1, height: 30, fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                placeholder ="Last name"
                value={this.props.last_name}
                onChangeText={(last_name) => this.props.typeLastName(last_name)}
              />
              </View>
              <LightText style={{ color: 'red', marginTop: 5 }}>{this.props.errors.last_name ? this.props.errors.last_name : null}</LightText>
              <View style={{ height: 1, backgroundColor: colors.gray05}}/>
          </View>
        </View>
        <View style={{marginTop: 10, height: 1, backgroundColor: colors.gray05}}/>
{/* Gender */}
        <View style={{ marginVertical: 25 }}>
           <View style={{ flexDirection:'row', marginHorizontal: 15, alignItems:'center'}}>
            <Image
                source={require("../../assets/gender.png")}
                style={{ tintColor: colors.serviceProvider, height: 20, width: 20}}
                resizeMode={'contain'}
              />
              <LightText style={{ color: colors.serviceProvider, marginLeft: 10, fontSize: 16}}>Gender</LightText>
           </View>
           <View style={{ flexDirection:'row', marginTop: 20, justifyContent:'space-around', marginHorizontal:20}}>
            <OptionButton
                style={{ flex: 2 }}
                value={'Female'}
                active= {(this.props.gender === 2) ? true : false}
                color={colors.serviceProvider}
                onPress={ () => this.props.selectGender(2)}
              />
              <View style={{flex:1}}/>
              <OptionButton
                style={{ flex: 2 }}
                value={'Male'}
                active= {(this.props.gender === 1) ? true : false}
                color={colors.serviceProvider}
                onPress={ () => this.props.selectGender(1)}
              />
           </View>
        </View>
        <View style={{ height: 1, backgroundColor: colors.gray05}}/>
{/* Age */}
          <View style={{ marginVertical: 25 }}>
           <View style={{ flexDirection:'row', marginHorizontal: 15, alignItems:'center'}}>
            <Image
                source={require("../../assets/calendar-simple-line-icons.png")}
                style={{ tintColor: colors.serviceProvider, height: 20, width: 20 }}
                resizeMode={'contain'}
              />
              <LightText style={{ color: colors.serviceProvider, marginLeft: 10, fontSize: 16}}>Age Group</LightText>
           </View>
           <View style={{ flexDirection:'row', marginVertical: 20, justifyContent:'space-around', marginHorizontal:20}}>
            <OptionButton
                style={{ flex: 2 }}
                value={'Less than 50'}
                active= {(this.props.age_group === 1) ? true : false}
                color={colors.serviceProvider}
                onPress={ () => this.props.selectAgeGroup(1)}
              />
              <View style={{flex:1}}/>
              <OptionButton
                style={{ flex: 2 }}
                value={'50 to 70'}
                active= {(this.props.age_group === 2) ? true : false}
                color={colors.serviceProvider}
                onPress={ () => this.props.selectAgeGroup(2)}
              />
           </View>
           <View style={{ flexDirection:'row', marginVertical: 10, justifyContent:'space-around', marginHorizontal:20}}>
              
           <OptionButton
                style={{ flex: 2 }}
                value={'70 to 90'}
                active= {(this.props.age_group === 3) ? true : false}
                color={colors.serviceProvider}
                onPress={ () => this.props.selectAgeGroup(3)}
              />
              <View style={{flex:1}}/>
              <OptionButton
                style={{ flex: 2 }}
                value={'Above 90'}
                active= {(this.props.age_group === 4) ? true : false}
                color={colors.serviceProvider}
                onPress={ () => this.props.selectAgeGroup(4)}
              />
           </View>
        </View>
        <View style={{marginTop: 10, height: 1, backgroundColor: colors.gray05}}/>
{/* Service */}

            <View style={{ marginVertical: 25 }}>
                            <View style={{ flexDirection:'row', marginHorizontal: 15, alignItems:'center'}}>
                                <Image
                                    source={require("../../assets/flag.png")}
                                    style={{ tintColor: colors.serviceProvider, height: 20, width: 20 }}
                                    resizeMode={'contain'}
                                />
                                <LightText style={{ color: colors.serviceProvider, marginLeft: 10, fontSize: 16}}>Services</LightText>
                            </View>
                                <FlatList
                                style={{ marginTop: 20, marginHorizontal:20 }}
                                data={servicesData}
                                numColumns={2}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item, index}) =>(
                                 
                                    <View style={{ padding: 10 }}>
                                        <OptionButton
                                        style={{ flex: 2 }}
                                        value={item.name}
                                        active= {(this.props.service.includes(item.id)) ? true : false}
                                        color={colors.serviceProvider}
                                        onPress={ () => this.props.selectService(item.id)}
                                    />
                                </View>
                                )}
                                />
                            </View>
                            <View style={{ height: 1, backgroundColor: colors.gray05}}/>

{/* Continue Button */}
        <View>
          <Button
          value={'Continue'}
          color={colors.serviceProvider}
           onPress={()=> this.props.navigateToServiceProviderDetails(this.props.first_name, this.props.last_name, this.props.navigation)}
          />
         </View>
        </View>
      </ScrollView>
      </SafeAreaView>
      </View>
    )
  } 
}

const mapStateToProps = state =>{
  return{
    first_name: state.user.first_name,
    last_name: state.user.last_name,
    gender: state.user.gender,
    age_group: state.user.age_group,
    service: state.user.service,
    profile_picture: state.user.profile_image,
    errors: state.user.errors,
    isEdit: state.user.isEdit 
  }
};

const mapDispatchToProps = dispatch =>{
  return{
    typeFirstName: (first_name) => {
      dispatch(typeFirstName(first_name));
    },
    typeLastName: (last_name) => {
      dispatch(typeLastName(last_name));
    },
    selectGender: (gender_id) => {
      dispatch(selectGender(gender_id));
    },
    selectAgeGroup: (age_group) => {
      dispatch(selectAgeGroup(age_group));
    },
    selectService: (service_id) => {
      dispatch(selectService(service_id));
    },
    selectProfileImage:(profile_picture) =>{
      dispatch(selectProfileImage(profile_picture));
    },
    navigateToServiceProviderDetails:(first_name, last_name, navigation) => navigateToServiceProviderDetails(first_name, last_name, dispatch,navigation)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ServiceProviderProfile);