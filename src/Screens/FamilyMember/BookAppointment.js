import React, { Component } from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  View,
  SafeAreaView,
} from "react-native";
import { connect } from 'react-redux';
import API from '../../Components/API';
import {removePreBooked} from '../../store/actions/Services';
import {LightText} from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import {Button} from '../../Components/button';


const width = Dimensions.get('window').width;

class BookAppointment extends Component {
    constructor() {
      super();
      this.state = {
        first_name:'',
        last_name:'',
        image:'',
        phone_number:'',
        gender:1,
        age_group:1,
        service:1,
        error:{}
      }
    }

    render(){
        return (
            <View style={{ flex: 1}}>
                <SafeAreaView style={{ flex: 1}}>
                    <View style={{flex: .08, backgroundColor:'#fff'}}>
                        <Header
                        leftNavigation={this.props.navigation}
                        color={colors.familyMember}
                        value={'Book Appointment'}
                        />
                    </View>

                    <ScrollView style={{ flex: 1}}>
                                <View style={{ flex: 1 ,marginHorizontal: 10}}>

{/* Profile Pic */}
                                    <View style={{ justifyContent:'center', alignItems:'center',marginVertical: 10 }}>
                                                <View style={{ justifyContent:'flex-start', alignItems:'flex-end'}}>
                                                <View style={{ height: width/3, width: width/3, borderRadius: (width/3)/ 2, borderWidth: 2, borderColor: colors.familyMember, justifyContent:'center', alignItems:'center' }}>
                                                <Image
                                                    style={{ height: width/3 - 10, width: width/3 - 10, borderRadius: (width/3 - 10)/ 2 }}
                                                    source={(this.props.navigation.state.params.patient.image) ? ({uri: `${API.baseURL}${this.props.navigation.state.params.patient.image}` }) : require("../../assets/profile.png")}
                                                />
                                                </View>
                                                </View>
                                                <LightText style={{ color: colors.familyMember, marginVertical: 10 }}>{`${this.props.navigation.state.params.patient.first_name} ${this.props.navigation.state.params.patient.last_name}`}</LightText>
                                                <LightText>Patient</LightText>           
                                            </View>

                                        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
{/* Text Details */}
                                    
                                    <View style={{ flex: 2, marginVertical: 10, marginHorizontal:10 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', marginVertical: 10, marginHorizontal: 10}}>
                                            <Image
                                                style={{height:15,width:15, tintColor: colors.familyMember}}
                                                source={require("../../assets/phone.png")}
                                                />
                                                <LightText style={{ marginLeft: 15, height: 30}}>{`${this.props.navigation.state.params.patient.contact_number}`}</LightText>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', marginVertical: 10, marginHorizontal: 10}}>
                                            <Image
                                                style={{height:15,width:15, tintColor: colors.familyMember}}
                                                source={require("../../assets/gender.png")}
                                                />
                                                <LightText style={{ marginLeft: 15, height: 30}}>{this.props.navigation.state.params.patient.gender === 1 ? 'Male' : 'Female'}</LightText>
                                        </View>
                                        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
                                    </View>

                                    <View style={{ marginVertical: 20 }}>
                                            <View style={{ marginHorizontal: 15,marginVertical: 5}}>
                                            <View style={{ flexDirection:'row', marginHorizontal: 10, alignItems:'center'}}>
                                                <Image
                                                    source={require("../../assets/calendar-simple-line-icons.png")}
                                                    style={{ tintColor: colors.familyMember, height: 20, width: 20 }}
                                                    resizeMode={'contain'}
                                                />
                                                <LightText style={{ color: colors.familyMember, marginLeft: 10, fontSize: 16}}>Age Group</LightText>
                                                </View>
                                                <View style={{marginHorizontal: 40, marginVertical:10}}>
                                                    <LightText>50 to 70</LightText>
                                                </View>
                                                <View style={{marginTop: 5, height: 1, backgroundColor: colors.gray05}}/>
                                            </View>
                                            </View>

{/* Book Button */}
                                        <View style={{flex:1}}>
                                        <Button
                                        value={'Book Appointment'}
                                        color={colors.familyMember}
                                        onPress={()=>{
                                            console.log('patient id', this.props.navigation.state.params.patient.id)
                                            this.props.removePreBooked();
                                            this.props.navigation.navigate('FMLocation',{patientId:this.props.navigation.state.params.patient.id})
                                        }}
                                        />
                                        </View>

                                </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }
}

const mapStateToProps = state =>{
    return{}
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
        removePreBooked:() => removePreBooked(dispatch)
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(BookAppointment);