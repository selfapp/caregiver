import React, { Component } from "react";
import {
  Dimensions,
  Image,
  View,
  FlatList,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { connect } from 'react-redux';
import { 
  navigateToPayment,
  fetchFamilyMemberList,
  fetchFamilyPatientList
} from '../../store/actions/user';
import API from '../../Components/API';
import { BoldText, LightText} from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import {Button} from '../../Components/button';

class FamilyDetails extends Component {
  constructor() {
    super();
    this.state = {
      gender:1,
      ageGroup:1,
      service:1
    };
  }

  componentDidMount(){
     this.props.fetchFamilyMemberList();
     this.props.fetchFamilyPatientList();
  }



  render(){
    return(

  <View style={{ flex: 1}}>
    <SafeAreaView style={{ flex: 1}}>
      <View style={{flex: .08, backgroundColor:'#fff'}}>
            <Header
              leftNavigation={this.props.navigation}
              color={colors.familyMember}
              value={'Profile'}
            />
        </View>

      {/* <ScrollView style={{ flex: 1}}> */}
        <View style={{marginHorizontal: 10, flex:1}}>

{/* Family members */}
        <View style={{ marginVertical: 10, marginHorizontal: 10, flex:1}}>
            <View style={{ flex: 1}}>
            <View style={{ backgroundColor:'#fff', flexDirection: 'row', alignItems:'center', justifyContent:'space-between', padding: 8, borderColor: colors.gray05}}>
                    <View style={{ flexDirection:'row'}}>
                    <Image
                        style={{ height: 25, width: 25, borderRadius: 25/2, tintColor: colors.familyMember }}
                        source={require("../../assets/member-icon.png")}
                        resizeMode={'center'}
                    />
                    <BoldText style={{ marginLeft: 10, color: colors.familyMember, fontSize: 14}}>Family Member Info</BoldText>
                    </View>
                    <TouchableOpacity style={{ height: 30, width: 30, alignItems:'center', justifyContent:'center'}}
                      onPress={() => this.props.navigation.navigate('AddFMember')}
                    >
                      <Image
                        source={require('../../assets/plusIcon.png')}
                        style={{ tintColor:'red'}}
                      />
                    </TouchableOpacity>
                </View>
                <View style={{ height: 1, backgroundColor: colors.gray05}}/>
          <FlatList
            data={this.props.familyMemberList}
            keyExtractor = {(item, index) => index.toString()}
            renderItem = {({item, index})=>(
                <View style={{ borderColor: colors.gray05}}>
                    <View style={{ marginHorizontal: 20, marginVertical: 10, flexDirection: 'row', alignItems:'center' }}>
                    <View style={{ height: 60, width: 60, borderRadius: 30, borderColor: colors.familyMember, borderWidth: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image
                            style={{ height: 50, width: 50, borderRadius: 25 }}
                            source={ (item.image) ? ({uri: `${API.baseURL}${item.image}` }) : require("../../assets/family-member.png")}
                        />
                    </View>
                    <BoldText style={{ fontSize: 16, marginLeft: 15}}>{item.first_name + ' ' + item.last_name}</BoldText>
                    </View>
                    <View style={{ height: 1, backgroundColor: colors.gray05}}/>
                </View>
            )}
            ListEmptyComponent = {() =>(
              <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
                  <LightText style={{ padding: 20, textAlign:'center', color: colors.familyMember, justifyContent: "center", fontSize: 14 }}>No Family member added</LightText>
            </View>
            )}
          />
          </View>

          
 {/* Patient List          */}
    <View style={{ flex: 1}}>
          <View style={{ backgroundColor:'#fff', flexDirection: 'row', alignItems:'center', justifyContent:'space-between', padding: 8, borderColor: colors.gray05}}>
                    <View style={{ flexDirection:'row'}}>
                    <Image
                        style={{ height: 25, width: 25, borderRadius: 25/2, tintColor: colors.familyMember }}
                        source={require("../../assets/member-icon.png")}
                        resizeMode={'center'}
                    />
                    <BoldText style={{ marginLeft: 10, color: colors.familyMember, fontSize: 14}}>Patients List</BoldText>
                    </View>
                    <TouchableOpacity style={{ height: 30, width: 30, alignItems:'center', justifyContent:'center'}}
                      onPress={() => this.props.navigation.navigate('AddPatient')}
                    >
                      <Image
                        source={require('../../assets/plusIcon.png')}
                        style={{ tintColor:'red'}}
                      />
                    </TouchableOpacity>
                </View>
                <View style={{ height: 1, backgroundColor: colors.gray05}}/>

          <FlatList
            data={this.props.familyPatientList}
            keyExtractor = {(item, index) => index.toString()}
            renderItem = {({item, index})=>(
                <View style={{ borderColor: colors.gray05}}>
                    <View style={{ marginHorizontal: 20, marginVertical: 10, flexDirection: 'row', alignItems:'center' }}>
                    <View style={{ height: 60, width: 60, borderRadius: 30, borderColor: colors.familyMember, borderWidth: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image
                            style={{ height: 50, width: 50, borderRadius: 25 }}
                            source={ (item.image) ? ({uri: `${API.baseURL}${item.image}` }) : require("../../assets/family-member.png")}
                        />
                    </View>
                    <BoldText style={{ fontSize: 16, marginLeft: 15}}>{item.first_name + ' ' + item.last_name}</BoldText>
                    </View>
                    <View style={{ height: 1, backgroundColor: colors.gray05}}/>
                </View>
            )}
            ListEmptyComponent = {() =>(
              <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
                  <LightText style={{ padding: 20, textAlign:'center', color: colors.familyMember, justifyContent: "center", fontSize: 14 }}>No patient added</LightText>
            </View>
            )}
          />
          </View>

        </View>

{/* Continue Button */}
        <View style={{ flex: .2}}>
          <Button
          value={'Continue'}
          color={colors.familyMember}
            onPress={()=> this.props.navigateToPayment(colors.familyMember, this.props.navigation)}
          />
         </View>
        </View>
      {/* </ScrollView> */}
      </SafeAreaView>
      </View>
    )
  } 
}

const mapStateToProps = state =>{
  return{
     familyMemberList: state.user.familyMemberList,
     familyPatientList: state.user.familyPatientList
  }
};

const mapDispatchToProps = dispatch =>{
  return{
    fetchFamilyMemberList:() => fetchFamilyMemberList(dispatch),
    fetchFamilyPatientList:() => fetchFamilyPatientList(dispatch),
    navigateToPayment:(color, navigation) => navigateToPayment(color, navigation)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FamilyDetails);