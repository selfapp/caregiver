import React, { Component } from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  TextInput,
  View,
  SafeAreaView,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  TouchableOpacity,
  Platform,
  Keyboard,
  FlatList,
  Alert
} from "react-native";
import Share from 'react-native-share';

import { connect } from 'react-redux';
import API from '../../Components/API';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import {fetchFamilyPatientList} from '../../store/actions/user';
import {LightText, BoldText} from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import {Button, OptionButton} from '../../Components/button';

const shareOptions = {
    title: 'Invitation from CareGiver',
    message: 'Join your family on CareGiver',
    url: 'http://caregiver.com',
};

const width = Dimensions.get('window').width;
const servicesData = [
    {
        name:'Eating',
        id:1
    },
    {
        name:'Bathing',
        id:2
    },
    {
        name:'Toileting',
        id:3
    },
    {
        name:'Dressing',
        id:4
    },
    {
        name:'Transferring from chair to Bed or vice versa',
        id:5
    },
    {
      name:'Walking or moving about',
      id:6
  },
]

class AddPatient extends Component {
    constructor() {
      super();
      this.state = {
        first_name:'',
        last_name:'',
        image:'',
        phone_number:'',
        gender:1,
        age_group:1,
        service:1,
        error:{}
      }
    }

    openGalleryForProfilePic(buttonIndex) {
    
        if(buttonIndex === 1) {
            ImagePicker.openPicker({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                   this.setState({image: `data:${image.mime};base64,${image.data}`})
                });
        } else if(buttonIndex === 0) {
            ImagePicker.openCamera({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    this.setState({image: `data:${image.mime};base64,${image.data}`})
                });
        }
    }
      showActionSheet = () => {
        this.ActionSheet.show()
      }

    onContinue(){
        const { first_name, last_name,image,phone_number, gender, age_group, service} = this.state;
        if(first_name && last_name && phone_number && first_name.length > 0 && last_name.length > 0 && phone_number.length === 10){
            API.addPatient(first_name, last_name, image, phone_number, gender, age_group, service)
            .then((res) => res.json())
            .then(jsonRes =>{
                console.log(jsonRes)
                if(jsonRes.status){
                    this.props.fetchFamilyPatientList();
                    this.props.navigation.goBack();
                }else{
                    if(jsonRes.invite === '0'){
                        Alert.alert("",
                            `${jsonRes.message}`,
                            [
                              {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                             
                              {text: 'Invite', onPress: () => Share.open(shareOptions)},
                            ],
                            {cancelable: false},
                          );
                    }else{
                        alert(jsonRes.message)
                    }
                
                }
            })
            .catch(err => {
                alert(err)
                console.log(err);
            })
        }else{
            let error = ''
            if(!first_name){
                error = 'Kindly fill the first name'
            }else if(!last_name) {
                error = 'Kindly fill the last name';
            }else if(!phone_number){
                error = 'Kindly fill the phone number';
            }else if(phone_number.length != 10){
                error = 'Kindly fill the correct phone number';
            }
            alert(error)
            //this.setState({error: errors});
        }
    }

    render(){
        return (
            <View style={{ flex: 1}}>
                <SafeAreaView style={{ flex: 1}}>
                    <View style={{flex: .08, backgroundColor:'#fff'}}>
                        <Header
                        leftNavigation={this.props.navigation}
                        color={colors.familyMember}
                        value={'Add new patient'}
                        />
                    </View>

                    <ScrollView style={{ flex: 1}}>
                        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                            {/* <KeyboardAvoidingView
                            behavior={Platform.OS === "ios" ? "height" : null}
                            style={{flex: 1}}
                            keyboardVerticalOffset={Platform.select({ios: -20, android: 20})}
                            > */}
                                <View style={{ flex: 1 ,marginHorizontal: 10}}>

{/* Profile Pic */}
                                    <View style={{ justifyContent:'center', alignItems:'center',marginVertical: 10 }}>
                                                <View style={{ justifyContent:'flex-start', alignItems:'flex-end'}}>
                                                <View style={{ height: width/3, width: width/3, borderRadius: (width/3)/ 2, borderWidth: 2, borderColor: colors.familyMember, justifyContent:'center', alignItems:'center' }}>
                                                <Image
                                                    style={{ height: width/3 - 10, width: width/3 - 10, borderRadius: (width/3 - 10)/ 2 }}
                                                    source={(this.state.image) ? ({uri: this.state.image}) : require("../../assets/profile.png")}
                                                />
                                                </View>
                                                <View style={{ position:'absolute', borderRadius:20, alignItems:'center', justifyContent:'center', height:40, width:40, backgroundColor:'#fff'}}>
                                                    <TouchableOpacity
                                                    onPress={() => { this.showActionSheet()}}
                                                    >
                                                    <Image
                                                    source={require('../../assets/edit.png')}
                                                    style={{ height: 25, width: 25, tintColor:colors.familyMember}}
                                                    />
                                                    </TouchableOpacity>
                                                </View>
                                                </View>
                                                <LightText style={{ color: colors.familyMember, marginVertical: 10 }}>Upload photo</LightText>
                                                <LightText>Patient</LightText>           
                                            </View>
{/* Actionsheet For Option */}
                                            <ActionSheet
                                                ref={o => this.ActionSheet = o}
                                                title={'Which one do you like ?'}
                                                options={['Take photo', 'Choose from Library', 'Cancel']}
                                                cancelButtonIndex={2}
                                                onPress={(index) => { this.openGalleryForProfilePic(index) }}
                                                />
                                        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
{/* Text Details */}
                                    <View style={{ flexDirection: 'row', alignItems:'center', marginVertical: 20}}>
                                        <Image
                                            style={{ marginLeft:3, tintColor: colors.familyMember}}
                                            source={require("../../assets/member-icon.png")}
                                        />
                                        <LightText style={{ color: colors.familyMember, marginLeft: 5}}> Patient name</LightText>
                                    </View>
                                    <View style={{ marginVertical: 20 }}>
                                            <View style={{ marginHorizontal: 15,marginVertical: 5}}>
                                                <View style={{ flexDirection: 'row', alignItems:'center'}}>
                                                <Image
                                                style={{height:15,width:15, tintColor: colors.familyMember}}
                                                source={require("../../assets/name-icon.png")}
                                                />
                                                <TextInput
                                                    style={{ flex: 1, height: 30, fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                                    placeholder ="First name"
                                                    value={this.state.first_name}
                                                    onChangeText={(first_name) => this.setState({first_name: first_name})}
                                                />
                                                </View>
                                                <View style={{marginTop: 5, height: 1, backgroundColor: colors.gray05}}/>
                                            </View>

                                            <View style={{ marginHorizontal: 15, marginTop: 10}}>
                                                <View style={{ flexDirection: 'row', alignItems:'center'}}>
                                                <Image
                                                style={{height:15,width:15, tintColor: colors.familyMember}}
                                                source={require("../../assets/name-icon.png")}
                                                />
                                                <TextInput
                                                    style={{flex: 1, height: 30, fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                                    placeholder ="Last name"
                                                    value={this.state.last_name}
                                                    onChangeText={(last_name) => this.setState({last_name:last_name})}
                                                />
                                                </View>
                                                <View style={{marginTop: 10, height: 1, backgroundColor: colors.gray05}}/>
                                            </View>
                                            </View>
                                    <View style={{ flexDirection: 'row', alignItems:'center', marginVertical: 20}}>
                                        <Image
                                            style={{ marginLeft:3, tintColor: colors.familyMember}}
                                            source={require("../../assets/phone.png")}
                                        />
                                        <LightText style={{ color: colors.familyMember, marginLeft: 5}}> Phone Number</LightText>
                                    </View>

                                    <View style={{ marginHorizontal: 15, marginTop: 10}}>
                                                <TextInput
                                                    style={{fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                                    placeholder ="Phone number"
                                                    maxLength={10}
                                                    keyboardType={'number-pad'}
                                                    value={this.state.phone_number}
                                                    onChangeText={(phone_number) => this.setState({phone_number:phone_number})}
                                                />
                                                <View style={{marginTop: 10, height: 1, backgroundColor: colors.gray05}}/>
                                            </View>

{/* Gender */}
        <View style={{ marginVertical: 25 }}>
           <View style={{ flexDirection:'row', marginHorizontal: 10, alignItems:'center'}}>
            <Image
                source={require("../../assets/gender.png")}
                style={{ tintColor: colors.familyMember, height: 20, width: 20}}
                resizeMode={'contain'}
              />
              <LightText style={{ color: colors.familyMember, marginLeft: 10, fontSize: 16}}>Gender</LightText>
           </View>
           <View style={{ flexDirection:'row', marginTop: 20, justifyContent:'space-around', marginHorizontal:20}}>
            <OptionButton
                style={{ flex: 2 }}
                value={'Female'}
                active= {(this.state.gender === 2) ? true : false}
                color={colors.familyMember}
                onPress={ () => this.setState({gender:2})}
              />
              <View style={{flex:1}}/>
              <OptionButton
                style={{ flex: 2 }}
                value={'Male'}
                active= {(this.state.gender === 1) ? true : false}
                color={colors.familyMember}
                onPress={ () => this.setState({gender:1})}
              />
           </View>
        </View>
        <View style={{ height: 1, backgroundColor: colors.gray05}}/>
{/* Age */}
          <View style={{ marginVertical: 25 }}>
           <View style={{ flexDirection:'row', marginHorizontal: 10, alignItems:'center'}}>
            <Image
                source={require("../../assets/calendar-simple-line-icons.png")}
                style={{ tintColor: colors.familyMember, height: 20, width: 20 }}
                resizeMode={'contain'}
              />
              <LightText style={{ color: colors.familyMember, marginLeft: 10, fontSize: 16}}>Age Group</LightText>
           </View>
           <View style={{ flexDirection:'row', marginVertical: 20, justifyContent:'space-around', marginHorizontal:20}}>
            <OptionButton
                style={{ flex: 2 }}
                value={'Less than 50'}
                active= {(this.state.age_group === 1) ? true : false}
                color={colors.familyMember}
                onPress={ () => this.setState({age_group:1})}
              />
              <View style={{flex:1}}/>
              <OptionButton
                style={{ flex: 2 }}
                value={'50 to 70'}
                active= {(this.state.age_group === 2) ? true : false}
                color={colors.familyMember}
                onPress={ () => this.setState({age_group:2})}
              />
           </View>
           <View style={{ flexDirection:'row', marginVertical: 10, justifyContent:'space-around', marginHorizontal:20}}>
              
           <OptionButton
                style={{ flex: 2 }}
                value={'70 to 90'}
                active= {(this.state.age_group === 3) ? true : false}
                color={colors.familyMember}
                onPress={ () => this.setState({age_group:3})}
              />
              <View style={{flex:1}}/>
              <OptionButton
                style={{ flex: 2 }}
                value={'Above 90'}
                active= {(this.state.age_group === 4) ? true : false}
                color={colors.familyMember}
                onPress={ () => this.setState({age_group:4})}
              />
           </View>
        </View>
        <View style={{marginTop: 10, height: 1, backgroundColor: colors.gray05}}/>

{/* Service */}

            <View style={{ marginVertical: 25 }}>
                            <View style={{ flexDirection:'row', marginHorizontal: 10, alignItems:'center'}}>
                                <Image
                                    source={require("../../assets/flag.png")}
                                    style={{ tintColor: colors.familyMember, height: 20, width: 20 }}
                                    resizeMode={'contain'}
                                />
                                <LightText style={{ color: colors.familyMember, marginLeft: 10, fontSize: 16}}>Services</LightText>
                            </View>
                                <FlatList
                                style={{ marginTop: 20, marginHorizontal:20 }}
                                data={servicesData}
                                numColumns={2}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item, index}) =>(
                                    <View style={{ padding: 10 }}>
                                        <OptionButton
                                        style={{ flex: 2 }}
                                        value={item.name}
                                        active= {(this.state.service === index + 1) ? true : false}
                                        color={colors.familyMember}
                                        onPress={ () => this.setState({service: index + 1})}
                                    />
                                </View>
                                )}
                                />
                            </View>
                            <View style={{ height: 1, backgroundColor: colors.gray05}}/>

{/* Continue Button */}
                                        <View>
                                        <Button
                                        value={'Continue'}
                                        color={colors.familyMember}
                                        onPress={()=>this.onContinue()}
                                        />
                                        </View>

                                </View>
                            {/* </KeyboardAvoidingView> */}
                        </TouchableWithoutFeedback>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }
}

const mapStateToProps = state =>{
    return{}
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
        fetchFamilyPatientList:() => fetchFamilyPatientList(dispatch)
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(AddPatient);