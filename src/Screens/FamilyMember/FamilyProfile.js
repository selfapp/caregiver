import React, { Component } from "react";
import {
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView
} from "react-native";
import { connect } from 'react-redux';
import { 
  navigateToFamilyDetails,
  typeFirstName, 
  typeLastName,
  selectProfileImage,
  updateFamilyMemberProfile
 } from '../../store/actions/user';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet'
import {LightText} from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import {Button} from '../../Components/button';
// ({uri: this.props.profile_picture})
import API from '../../Components/API';
const width = Dimensions.get('window').width;

class FamilyProfile extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  componentWillMount() {
  }

  componentWillUnmount() {
  }
  openGalleryForProfilePic(buttonIndex) {
    
    if(buttonIndex === 1) {
        ImagePicker.openPicker({
            width: 1000,
            height: 1000,
            cropping: true,
            includeBase64: true,
            mediaType: 'photo'
        })
            .then(image => {
               this.props.selectProfileImage(`data:${image.mime};base64,${image.data}`)
            });
    } else if(buttonIndex === 0) {
        ImagePicker.openCamera({
            width: 1000,
            height: 1000,
            cropping: true,
            includeBase64: true,
            mediaType: 'photo'
        })
            .then(image => {
               this.props.selectProfileImage(`data:${image.mime};base64,${image.data}`)
            });
    }
}
  showActionSheet = () => {
    this.ActionSheet.show()
  }

  render(){
    return(

  <View style={{ flex: 1}}>
    <SafeAreaView style={{ flex: 1}}>
      <View style={{flex: .08, backgroundColor:'#fff'}}>
            <Header
              leftNavigation={this.props.navigation}
              color={colors.familyMember}
              value={this.props.isEdit ? 'Edit Profile' : 'Profile'}
            />
        </View>
        <ScrollView style={{ flex: 1}}>
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <KeyboardAvoidingView
             behavior={Platform.OS === "ios" ? "height" : null}
             style={{flex: 1}}
             keyboardVerticalOffset={Platform.select({ios: -20, android: 20})}
            >
        
        <View style={{ flex: 1 ,marginHorizontal: 10}}>
        
{/* Profile Details */}
        <View style={{ flexDirection: 'row', alignItems:'center', marginVertical: 20}}>
          <Image
            style={{ marginLeft:3, tintColor: colors.familyMember}}
            source={require("../../assets/tell-us.png")}
          />
          <LightText style={{ color: colors.familyMember, marginLeft: 5}}> Tell us bit about yourself</LightText>
        </View>

{/* Profile Pic */}


      <View style={{ justifyContent:'center', alignItems:'center',marginVertical: 10 }}>
            <View style={{ justifyContent:'flex-start', alignItems:'flex-end'}}>
            <View style={{ height: width/3, width: width/3, borderRadius: (width/3)/ 2, borderWidth: 2, borderColor: colors.familyMember, justifyContent:'center', alignItems:'center' }}>
             {
               this.props.profile_picture?
               <Image
                style={{ height: width/3 - 10, width: width/3 - 10, borderRadius: (width/3 - 10)/ 2 }}
                source={(this.props.profile_picture[0]== '/') ? ({ uri: `${API.baseURL}${this.props.profile_picture}` }):({ uri: this.props.profile_picture })  }
              />:
              <Image
                style={{ height: width/3 - 10, width: width/3 - 10, borderRadius: (width/3 - 10)/ 2 }}
                source={require("../../assets/profile.png")}
              />
             }
            
            </View>
            <View style={{ position:'absolute', borderRadius:20, alignItems:'center', justifyContent:'center', height:40, width:40, backgroundColor:'#fff'}}>
                <TouchableOpacity
                  onPress={() => { this.showActionSheet()}}
                 >
                <Image
                  source={require('../../assets/edit.png')}
                  style={{ height: 25, width: 25, tintColor:colors.familyMember}}
                />
                </TouchableOpacity>
              </View>
              </View>
              <LightText style={{ color: colors.familyMember, marginVertical: 10 }}>Upload photo</LightText>
              <LightText>Family Member</LightText>           
        </View>
{/* Actionsheet For Option */}
              <ActionSheet
                  ref={o => this.ActionSheet = o}
                  title={'Which one do you like ?'}
                  options={['Take photo', 'Choose from Library', 'Cancel']}
                  cancelButtonIndex={2}
                  onPress={(index) => { this.openGalleryForProfilePic(index) }}
                  />
        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
{/* Text Details */}


<View style={{ marginVertical: 20 }}>
          <View style={{ marginHorizontal: 15,marginVertical: 5}}>
            <View style={{ flexDirection: 'row', alignItems:'center'}}>
            <Image
              style={{height:15,width:15, tintColor: colors.familyMember}}
              source={require("../../assets/name-icon.png")}
            />
              <TextInput
                style={{ flex: 1, height: 30, fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                placeholder ="First name"
                value={this.props.first_name}
                onChangeText={(first_name) => this.props.typeFirstName(first_name)}
              />
              </View>
              <LightText style={{ color: 'red', marginTop: 5 }}>{this.props.errors.first_name ? this.props.errors.first_name : null}</LightText>
              <View style={{marginTop: 5, height: 1, backgroundColor: colors.gray05}}/>
          </View>

          <View style={{ marginHorizontal: 15, marginTop: 10}}>
            <View style={{ flexDirection: 'row', alignItems:'center'}}>
            <Image
              style={{height:15,width:15, tintColor: colors.familyMember}}
              source={require("../../assets/name-icon.png")}
            />
              <TextInput
                style={{ flex: 1, height: 30, fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                placeholder ="Last name"
                value={this.props.last_name}
                onChangeText={(last_name) => this.props.typeLastName(last_name)}
              />
              </View>
              <LightText style={{ color: 'red', marginTop: 5 }}>{this.props.errors.last_name ? this.props.errors.last_name : null}</LightText>
              <View style={{marginTop: 10, height: 1, backgroundColor: colors.gray05}}/>
          </View>
        </View>

{/* Continue Button */}
        <View>
          <Button
          value={'Continue'}
          color={colors.familyMember}
          onPress={() => this.props.updateFamilyMemberProfile(this.props.first_name, this.props.last_name, this.props.profile_picture, this.props.navigation)}
          // onPress={()=> this.props.navigateToFamilyDetails(this.props.navigation)}
          />
         </View>
         
        </View>
        </KeyboardAvoidingView>
         </TouchableWithoutFeedback>
         </ScrollView>
         </SafeAreaView>
      </View>
    )
  } 
}

const mapStateToProps = state =>{
  return{
    first_name: state.user.first_name,
    last_name: state.user.last_name,
    profile_picture: state.user.profile_image,
    errors: state.user.errors,
    isEdit: state.user.isEdit
  }
};

const mapDispatchToProps = dispatch =>{
  return{
    typeFirstName: (first_name) => {
      dispatch(typeFirstName(first_name));
    },
    typeLastName: (last_name) => {
      dispatch(typeLastName(last_name));
    },
    selectProfileImage:(profile_picture) =>{
      dispatch(selectProfileImage(profile_picture));
    },
    updateFamilyMemberProfile:(first_name, last_name, profile_picture, navigation) => updateFamilyMemberProfile(first_name, last_name, profile_picture,dispatch, navigation),
    navigateToFamilyDetails:(navigation) => navigateToFamilyDetails(navigation)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FamilyProfile);