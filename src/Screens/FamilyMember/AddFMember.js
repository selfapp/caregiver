import React, { Component } from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  TextInput,
  View,
  SafeAreaView,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  TouchableOpacity,
  Platform,
  Keyboard
} from "react-native";
import { connect } from 'react-redux';
import API from '../../Components/API';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import {fetchFamilyMemberList} from '../../store/actions/user';
import {LightText, BoldText} from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import {Button} from '../../Components/button';


const width = Dimensions.get('window').width;


class AddFMember extends Component {
    constructor() {
      super();
      this.state = {
        first_name:'',
        last_name:'',
        image:'',
        phone_number:'',
        error:{}
      }
    }

    openGalleryForProfilePic(buttonIndex) {
    
        if(buttonIndex === 1) {
            ImagePicker.openPicker({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                   this.setState({image: `data:${image.mime};base64,${image.data}`})
                });
        } else if(buttonIndex === 0) {
            ImagePicker.openCamera({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    this.setState({image: `data:${image.mime};base64,${image.data}`})
                });
        }
    }
      showActionSheet = () => {
        this.ActionSheet.show()
      }

    addSuccess(){
        this.props.fetchFamilyMemberList();
        this.props.navigation.goBack();
    }


    onContinue(){

        const { first_name, last_name,image,phone_number} = this.state;
        if(first_name && last_name && phone_number && first_name.length > 0 && last_name.length > 0 && phone_number.length === 10){
            API.addFamilyMember(first_name, last_name, image, phone_number)
            .then((res) => res.json())
            .then(jsonRes =>{
                console.log(JSON.stringify(jsonRes))
                if(jsonRes.status){
                    this.addSuccess();
                }else{
                    alert(jsonRes.message)
                }
            })
            .catch(err => {
                alert(err)
                console.log(err);
            })
        }else{
            let errors = ''
            if(!first_name){
                errors = 'Kindly fill the first name'
            }else if(!last_name) {
                errors = 'Kindly fill the last name';
            }else if(!phone_number){
                errors = 'Kindly fill the phone number';
            }else if(phone_number.length != 10){
                errors = 'Kindly fill the correct phone number';
            }
            alert(errors)
        }
    }

    render(){
        return (
            <View style={{ flex: 1}}>
                <SafeAreaView style={{ flex: 1}}>
                    <View style={{flex: .08, backgroundColor:'#fff'}}>
                        <Header
                        leftNavigation={this.props.navigation}
                        color={colors.familyMember}
                        value={'Add new family member'}
                        />
                    </View>

                    <ScrollView style={{ flex: 1}}>
                        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                            <KeyboardAvoidingView
                            behavior={Platform.OS === "ios" ? "height" : null}
                            style={{flex: 1}}
                            keyboardVerticalOffset={Platform.select({ios: -20, android: 20})}
                            >
                                <View style={{ flex: 1 ,marginHorizontal: 10}}>

{/* Profile Pic */}
                                    <View style={{ justifyContent:'center', alignItems:'center',marginVertical: 10 }}>
                                                <View style={{ justifyContent:'flex-start', alignItems:'flex-end'}}>
                                                <View style={{ height: width/3, width: width/3, borderRadius: (width/3)/ 2, borderWidth: 2, borderColor: colors.familyMember, justifyContent:'center', alignItems:'center' }}>
                                                <Image
                                                    style={{ height: width/3 - 10, width: width/3 - 10, borderRadius: (width/3 - 10)/ 2 }}
                                                    source={(this.state.image) ? ({uri: this.state.image}) : require("../../assets/profile.png")}
                                                />
                                                </View>
                                                <View style={{ position:'absolute', borderRadius:20, alignItems:'center', justifyContent:'center', height:40, width:40, backgroundColor:'#fff'}}>
                                                    <TouchableOpacity
                                                    onPress={() => { this.showActionSheet()}}
                                                    >
                                                    <Image
                                                    source={require('../../assets/edit.png')}
                                                    style={{ height: 25, width: 25, tintColor:colors.familyMember}}
                                                    />
                                                    </TouchableOpacity>
                                                </View>
                                                </View>
                                                <LightText style={{ color: colors.familyMember, marginVertical: 10 }}>Upload photo</LightText>
                                                <LightText>Family Member</LightText>           
                                            </View>
{/* Actionsheet For Option */}
                                            <ActionSheet
                                                ref={o => this.ActionSheet = o}
                                                title={'Which one do you like ?'}
                                                options={['Take photo', 'Choose from Library', 'Cancel']}
                                                cancelButtonIndex={2}
                                                onPress={(index) => { this.openGalleryForProfilePic(index) }}
                                                />
                                        <View style={{marginTop: 2, height: 1, backgroundColor: colors.gray05}}/>
{/* Text Details */}
                                    <View style={{ flexDirection: 'row', alignItems:'center', marginVertical: 20}}>
                                        <Image
                                            style={{ marginLeft:3, tintColor: colors.familyMember}}
                                            source={require("../../assets/member-icon.png")}
                                        />
                                        <LightText style={{ color: colors.familyMember, marginLeft: 5}}> Family Member Name</LightText>
                                    </View>
                                    <View style={{ marginVertical: 20 }}>
                                            <View style={{ marginHorizontal: 15,marginVertical: 5}}>
                                                <View style={{ flexDirection: 'row', alignItems:'center'}}>
                                                <Image
                                                style={{height:15,width:15, tintColor: colors.familyMember}}
                                                source={require("../../assets/name-icon.png")}
                                                />
                                                <TextInput
                                                    style={{fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                                    placeholder ="First name"
                                                    value={this.state.first_name}
                                                    onChangeText={(first_name) => this.setState({first_name: first_name})}
                                                />
                                                </View>
                                                <LightText style={{ color: 'red', marginTop: 5 }}>{this.state.error.first_name ? this.state.error.first_name : null}</LightText>
                                                <View style={{marginTop: 5, height: 1, backgroundColor: colors.gray05}}/>
                                            </View>

                                            <View style={{ marginHorizontal: 15, marginTop: 10}}>
                                                <View style={{ flexDirection: 'row', alignItems:'center'}}>
                                                <Image
                                                style={{height:15,width:15, tintColor: colors.familyMember}}
                                                source={require("../../assets/name-icon.png")}
                                                />
                                                <TextInput
                                                    style={{fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                                    placeholder ="Last name"
                                                    value={this.state.last_name}
                                                    onChangeText={(last_name) => this.setState({last_name:last_name})}
                                                />
                                                </View>
                                                <LightText style={{ color: 'red', marginTop: 5 }}>{this.state.error.last_name ? this.state.error.last_name : null}</LightText>
                                                <View style={{marginTop: 10, height: 1, backgroundColor: colors.gray05}}/>
                                            </View>
                                            </View>
                                    <View style={{ flexDirection: 'row', alignItems:'center', marginVertical: 20}}>
                                        <Image
                                            style={{ marginLeft:3, tintColor: colors.familyMember}}
                                            source={require("../../assets/phone.png")}
                                        />
                                        <LightText style={{ color: colors.familyMember, marginLeft: 5}}> Phone Number</LightText>
                                    </View>

                                    <View style={{ marginHorizontal: 15, marginTop: 10}}>
                                                <TextInput
                                                    style={{fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                                    placeholder ="Phone number"
                                                    maxLength={10}
                                                    value={this.state.phone_number}
                                                    onChangeText={(phone_number) => this.setState({phone_number:phone_number})}
                                                />
                                                <LightText style={{ color: 'red' }}>{this.state.error.phone_number ? this.state.error.phone_number : null}</LightText>
                                                <View style={{marginTop: 0, height: 1, backgroundColor: colors.gray05}}/>
                                            </View>
{/* Continue Button */}
                                        <View>
                                        <Button
                                        value={'Continue'}
                                        color={colors.familyMember}
                                        onPress={()=>this.onContinue()}
                                        />
                                        </View>

                                </View>
                            </KeyboardAvoidingView>
                        </TouchableWithoutFeedback>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }
}

const mapStateToProps = state =>{
    return{}
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
        fetchFamilyMemberList:() => fetchFamilyMemberList(dispatch),
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(AddFMember);