import React, {Component} from 'react';
import{
View,
SafeAreaView,
FlatList,
Image,
TouchableOpacity
}from 'react-native';
import { connect } from 'react-redux';
import { fetchFamilyPatientList} from '../../store/actions/user';
import API from '../../Components/API';
import Header from '../../Components/Header';
import { BoldText, LightText } from '../../Components/styledTexts';
import colors from '../../styles/colors';

class FMPatientList extends Component {
 constructor(props){
     super(props)
 }
 componentDidMount(){
    this.props.fetchFamilyPatientList();
 }

addPatient = () =>{
    this.props.navigation.navigate('AddPatient')
}


 renderItem = ({ item}) =>{
    return(
                <View style={{ borderColor: colors.gray05}}>
                    <TouchableOpacity
                        onPress={()=> this.props.navigation.navigate('BookAppointment', {patient: item})}
                    >
                    <View style={{ marginHorizontal: 20, marginVertical: 10, flexDirection: 'row', alignItems:'center' }}>
                    <View style={{ height: 60, width: 60, borderRadius: 30, borderColor: colors.familyMember, borderWidth: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image
                            style={{ height: 50, width: 50, borderRadius: 25 }}
                            source={ (item.image) ? ({uri: `${API.baseURL}${item.image}` }) : require("../../assets/family-member.png")}
                        />
                    </View>
                    <View style={{ flex:1, marginLeft: 15}}>
                    <BoldText style={{ fontSize: 16}}>{item.first_name + ' ' + item.last_name}</BoldText>
                    <LightText>{item.contact_number}</LightText>
                    </View>
                        <View>
                        <Image
                         source={require('../../assets/options-icon.png')}
                         style={{ tintColor: colors.familyMember}}
                        />
                        </View> 
                    </View>
                    </TouchableOpacity>
                    <View style={{ height: 1, backgroundColor: colors.gray05}}/>
                </View>
    );
}

empetyList =() =>{
    return (
        <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
            <LightText style={{ padding: 20, textAlign:'center', color: colors.familyMember, justifyContent: "center", fontSize: 20 }}>No patient added</LightText>
        </View>
    );
}
renderSeparater = () => {
    return(
        <View style={{ flex: 1, height: 1, backgroundColor: colors.gray05 }}></View>
    )
}


 render(){
     return(
        <View style={{ flex: 1}}>
            <SafeAreaView style={{ flex: 1}}>
                <View style={{flex: .08, backgroundColor:'#fff'}}>
                 <Header
                  color={colors.familyMember}
                  value={'Patients List'}
                  onPressRight={this.addPatient}
                  rightIcon={require('../../assets/plusIcon.png')}
                />
            </View>
            <View style={{ flex: 1}}>

                    <FlatList
                        data={this.props.familyPatientList}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {this.renderItem}
                        ListEmptyComponent = { this.empetyList}
                        ListEmptyComponent = { this.empetyList}
                        ItemSeparatorComponent = {this.renderSeparater}
                    />

                </View>
            </SafeAreaView>
         </View>
     )
 }
}

const mapStateToProps = state =>{
    return{
        familyPatientList: state.user.familyPatientList
    }
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
        fetchFamilyPatientList:() => fetchFamilyPatientList(dispatch)
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(FMPatientList);