import React, { Component } from "react";
import {
    Dimensions,
    Image,
    ScrollView,
    View,
    SafeAreaView,
    FlatList,
    TouchableOpacity,
    Text
} from "react-native";
import { connect } from 'react-redux';
import API from '../../Components/API';
import { removePreBooked } from '../../store/actions/Services';
import { LightText } from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import { Button } from '../../Components/button';
import MapView, {Marker, Circle, PROVIDER_GOOGLE} from 'react-native-maps';

const width = Dimensions.get('window').width;

class AppoinmentHome extends Component {
    constructor() {
        super();
        this.state = {
            moredetail:null,
            Mycolor:'#fff',
            latitude: 28.515038,
            longitude: 77.080476,
        }
    }
    componentDidMount(){
        const data = this.props.navigation.getParam('data');
        this.setState({
            latitude: parseFloat(data.lat),
            longitude: parseFloat(data.lng)
        })
        let url='appointments/'+data.id;
        console.log("-----------------",url)
        method ='GET',

        API.request(url,method,null).then(res=>res.json()).then((responseJson) => {
            console.log("------------------",JSON.stringify(responseJson));
            console.log(responseJson);
            this.setState({moredetail:responseJson})
        })

        if(this.props.user.userType === "Patient"){
            this.setState({Mycolor:colors.patient}) 
         }else{
             this.setState({Mycolor:colors.familyMember}) 
         }
    }

    render() {
        const data = this.props.navigation.getParam('data');
        const {moredetail,Mycolor} = this.state;
        console.log(data);
        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: .08, backgroundColor: '#fff' }}>
                        <Header
                            // leftNavigation={this.props.navigation}
                            color={Mycolor}
                            value={'Appoinment Details'}
                            leftNavigation={this.props.navigation}
                        />
                    </View>

                    <ScrollView style={{ flex: 1 }}>
                        <View style={{ padding: 15, justifyContent: 'center' }}>
                            <Text style={{ color: Mycolor, fontWeight: '600' }}>
                                {data.name}
                           </Text>
                        </View>
                        <View style={{ height: 1, backgroundColor: colors.gray05 }} />
                        <View style={{ height: 100, width: '100%', flexDirection: 'row', paddingVertical: 10, }}>
                            <View style={{ width: '20%', justifyContent: 'center', alignItems: "center" }}>
                                <Image style={{tintColor:Mycolor}}  source={require('../../assets/calendar-appoint.png')}></Image>
                            </View>
                            <View style={{ width: '80%', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray' }}>{data.time}</Text>
                                <Text style={{ color: 'gray' }}>Tuesday 20 Sep 2019</Text>
                            </View>
                        </View>
                        <View style={{ height: 1, backgroundColor: colors.gray05 }} />
                        <View style={{ height: 100, width: '100%', flexDirection: 'row', paddingVertical: 10, }}>
                            <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                <Image style={{tintColor:Mycolor}}  source={require('../../assets/location-map.png')}></Image>
                            </View>
                            <View style={{ width: '80%', justifyContent: 'center',  }}>
                                <Text style={{ color: 'gray' }}>
                                {data.address}
                                {/* {moredetail?(moredetail.appointment.service_provider.address):null} */}
                                </Text>
                            </View>
                        </View>
                        <View style={{ height: 1, backgroundColor: colors.gray05 }} />
                      
                        <View style={{height: 100,}}>
                        <Text style={{ color: Mycolor, fontWeight: '600', paddingLeft:20}}>
                               Patient Info
                           </Text>
                            <View style={{ width: '100%', flexDirection: 'row', paddingVertical: 10, }}>
                                <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{tintColor:Mycolor}}  source={require('../../assets/patient-user.png')}></Image>
                                </View>
                                <View style={{ width: '80%', justifyContent: 'center', }}>
                                    <Text style={{ color: 'gray' }}>{moredetail?(moredetail.appointment.service_provider.patient.first_name):null}</Text>
                                </View>
                            </View>
                            <View style={{ width: '100%', flexDirection: 'row', paddingVertical: 10, }}>
                                <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{tintColor:Mycolor}}  source={require('../../assets/contact.png')}></Image>
                                </View>
                                <View style={{ width: '80%', justifyContent: 'center', }}>
                                
                                <Text style={{ color: 'gray' }}>{moredetail?(moredetail.appointment.service_provider.patient.contact_number):null}</Text>
                                </View>
                            </View>
                            <View style={{ height: 1, backgroundColor: colors.gray05 }} />
                        </View>
                        <View style={{height: 150,}}>
                        <Text style={{ color: Mycolor, fontWeight: '600', paddingLeft:20}}>
                               Service provider Info
                           </Text>
                            <View style={{ width: '100%', flexDirection: 'row', paddingVertical: 10, }}>
                                <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{tintColor:Mycolor}}  source={require('../../assets/patient-user.png')}></Image>
                                </View>
                                <View style={{ width: '80%', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray' }}>{moredetail?(moredetail.appointment.service_provider.first_name +" " + moredetail.appointment.service_provider.last_name):null}</Text>
                                </View>
                            </View>
                            <View style={{ width: '100%', flexDirection: 'row', paddingVertical: 10, }}>
                                <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{tintColor:Mycolor}}  source={require('../../assets/contact.png')}></Image>
                                </View>
                                <View style={{ width: '80%', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray' }}>{moredetail?(moredetail.appointment.service_provider.contact_number):null}</Text>
                                </View>
                            </View>
                            <View style={{ width: '100%', flexDirection: 'row', paddingVertical: 10, }}>
                                <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{tintColor:Mycolor}}  source={require('../../assets/services.png')}></Image>
                                </View>
                                <View style={{ width: '80%', justifyContent: 'center', }}>
                                <Text style={{ color: 'gray' }}>{moredetail?(moredetail.appointment.service_provider.name):null}</Text>
                                </View>
                            </View>
                            
                        </View>
                        <View style={{height:300, width:'100%'}}>
                        <MapView
                style={{flex: 1}}
                provider={PROVIDER_GOOGLE}
                region={{
                    latitude: this.state.latitude,
      longitude: this.state.longitude,
                    latitudeDelta: 0.0722,
                    longitudeDelta: 0.1221,
                    // longitudeDelta:0.0722 * 300/50
                }}
            >
        
                <Circle center={{latitude: this.state.latitude,
      longitude: this.state.longitude,}} radius={300}
                        fillColor={'rgba(65, 145, 170, 0.26)'} strokeColor={'transparent'}/>
                <Marker key={1} coordinate={{latitude: this.state.latitude,
      longitude: this.state.longitude,}}
                />
                
            </MapView>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        removePreBooked: () => removePreBooked(dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AppoinmentHome);