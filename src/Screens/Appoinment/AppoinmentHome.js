import React, { Component } from "react";
import {
    Dimensions,
    Image,
    ScrollView,
    View,
    SafeAreaView,
    FlatList,
    TouchableOpacity,
    Text, RefreshControl
} from "react-native";
import { connect } from 'react-redux';
import API from '../../Components/API';
import { removePreBooked } from '../../store/actions/Services';
import { BoldText, LightText } from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import { Button } from '../../Components/button';
import Loader from '../../Components/Loader'
import {
    fetchTrips
} from '../../store/actions/Trips';

const width = Dimensions.get('window').width;

class AppoinmentHome extends Component {
    constructor() {
        super();
        this.state = {
            loading: false, // user list loading
            isRefreshing: false, //for pull to refresh
            Mycolor:'#fff'
        }
    }

    componentWillMount() {
        this.props.fetchTrips();

    }
    redirect(item){
        console.log(item)
        if(this.props.user.userType === "Patient"){
            this.props.navigation.navigate('PAppoinmentDetail', { data: item })
            this.setState({Mycolor:colors.patient}) 
         }else{
            this.props.navigation.navigate('AppoinmentDetail', { data: item })
         }
    }
    componentDidMount() {
        this.props.fetchTrips();
        console.log("user--------------------------",this.props.user)
        if(this.props.user.userType === "Patient"){
           this.setState({Mycolor:colors.patient}) 
        }else{
            this.setState({Mycolor:colors.familyMember}) 
        }
    }
    onRefreshList() {
        this.setState({ isRefreshing: true }); // true isRefreshing flag for enable pull to refresh indicator
        this.props.fetchTrips();
        this.setState({ isRefreshing: false });
    }

    handleLoadMore = () => {
        if (!this.state.loading) {
            this.page = this.page + 1; // increase page by 1
            this.props.fetchTrips();; // method for API call 
        }
    };
    renderItem = ({ item }) => {
         const {Mycolor} = this.state;
        console.log("render item", item)
        return (
            <View style={{ borderColor: colors.gray05 }}>
                <TouchableOpacity style={{ padding: 10, }} onPress={this.redirect.bind(this,item)}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ color: Mycolor, width: width - 100, fontWeight: '600' }}>{item.first_name + ' ' + 'has requested for '}{item.name}</Text>
                        <View style={{ height: 15, width: 100, position: 'absolute', right: 10, flexDirection: 'row', justifyContent: 'space-evenly' }}>
                            <Image source={require('../../assets/time.png')} style={{tintColor:Mycolor}}></Image>
                            <Text style={{ color: Mycolor, fontSize: 10 }}>{item.time}</Text>
                        </View>
                    </View>
                    <View style={{ height: 50, width: '100%', flexDirection: 'row', paddingVertical: 10, }}>
                        <View style={{ width: 20, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../assets/location-map.png')} style={{tintColor:Mycolor}}></Image>
                        </View>
                        <View style={{ width: width - 120, justifyContent: 'center', }}>
                            <Text style={{ color: 'gray', maxWidth:100, width:width - 130}}>{item.address} </Text>
                        </View>
                        <View style={{ height: 15, width: 100, position: 'absolute', right: 10, flexDirection: 'row', top: '51%', justifyContent: 'space-evenly' }}>
                            <Image source={require('../../assets/services.png')} style={{tintColor:Mycolor}}></Image>
                            <Text style={{ color: 'gray', fontSize: 10, textAlign: 'left' }}>{item.status.toUpperCase()}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={{ height: 1, backgroundColor: colors.gray05 }} />

            </View>
        );
    }

    empetyList = () => {
        return (
            <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
                <BoldText style={{ color: "red", justifyContent: "center", fontSize: 20 }}>
                    {" "}
                    No Items Found
        </BoldText>
            </View>
        );
    }
    render() {
        const {Mycolor} = this.state;
        const { trips } = this.props
        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: .08, backgroundColor: '#fff' }}>
                        <Header
                            // leftNavigation={this.props.navigation}
                            color={Mycolor}
                            value={'Available Trip'}
                        />
                    </View>
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={this.props.trips.TripsList}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={this.renderItem}
                            ListEmptyComponent={this.empetyList}
                            ItemSeparatorComponent={this.renderSeparater}
                            onRefresh={this.onRefreshList.bind(this)}
                            refreshing={this.state.isRefreshing}
                            onEndReachedThreshold={0.4}

                        />
                    </View>
                </SafeAreaView>
                <Loader loading={this.props.trips.loading} />
            </View>
        );

    }
}

const mapStateToProps = state => {
    return {
        trips: state.trip,
        user: state.user,
        loading:state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchTrips: () => fetchTrips(dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AppoinmentHome);