import React, {Component} from 'react';
import {View, Text,Platform} from 'react-native';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';
import { OTP_SUCCESS } from '../store/ActionTypes';

import { saveTokenServ } from "../store/actions/user";
let deviceType = false;
class Splash extends Component{
    constructor(props){
        super(props);
    }
   async componentWillMount(){
        const userToken = await AsyncStorage.getItem("accessToken");

        if(userToken){
            AsyncStorage.getItem('user').then((user) =>{
                user = JSON.parse(user);
                this.props.updateLocalData(user);
                if(user.first_name === null){
                    this.props.navigation.navigate('profileCreationNavigationOptions')
                }else{
this.getToken();
                    if(user.type === "Patient"){
                        this.props.navigation.navigate('PTabNavigator')
                    }else if( user.type === 'ServiceProvider'){
                        this.props.navigation.navigate('SpTabNavigator')
                    }else if(user.type === 'FamilyMember'){
                        this.props.navigation.navigate('FMTabNavigator')
                    }else{
                    this.props.navigation.navigate('profileCreationNavigationOptions')
                    }
                }
            })
            .catch(error =>{
                this.props.navigation.navigate('profileCreationNavigationOptions')
            })
        }else{
            this.props.navigation.navigate('loginNavigationOptions')
        }
    }
    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                await AsyncStorage.setItem('fcmToken', fcmToken);
                console.log("fcm Tokem",fcmToken);
                deviceType = !(Platform.OS === "ios")
fcmToken = await AsyncStorage.getItem('fcmToken');
console.log("--------------------------------------",deviceType,fcmToken);
this.props.saveTokenServ(fcmToken,deviceType)
            }
        }else{
            deviceType = !(Platform.OS === "ios")
fcmToken = await AsyncStorage.getItem('fcmToken');
console.log("--------------------------------------",deviceType,fcmToken);
this.props.saveTokenServ(fcmToken,deviceType)
        }
      }

    render(){
        return(
            <View style={{ flex: 1, alignItems:'center'}}>
            </View>
        )
    }
}

const mapStateToProps = state =>{
    return {
       
    }
};

const mapDispatchToProps = dispatch => {
    return {
       updateLocalData:(user) =>{
        dispatch({
            type: OTP_SUCCESS,
            payload: user
        });
       },
       saveTokenServ:( fcm, device_Type, navigation) => saveTokenServ(fcm, device_Type, dispatch, navigation)

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash);