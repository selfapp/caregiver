import React, {Component} from 'react';
import{
    View,
    StyleSheet,
    Image,
    Keyboard,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView
} from 'react-native';
import{ connect} from 'react-redux';
import { 
    navigateToPatientTab,
    navigateToFamilyMemberTab
} from '../store/actions/user';
import{Button} from '../Components/button';
import Header from '../Components/Header';
import colors from '../styles/colors';
import { BoldText, LightText } from '../Components/styledTexts';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { EDIT_PROFILE_SUCCESS } from '../store/ActionTypes';

class Payment extends Component{
    constructor(props){
        super(props);
        this.state = {
            cardNumber:''
        }
    }

    onContinue(){
        console.log('is edit value', this.props.isEdit)
        if(this.props.isEdit){
            this.props.editProfile()
            this.props.navigation.popToTop()
            // this.props.navigation.navigate('Services')
        }else{
            (this.props.navigation.state.params.color === colors.patient) ? (this.props.navigateToPatientTab(this.props.navigation)) : (this.props.navigateToFamilyMemberTab(this.props.navigation))
        }
    }


// add space after 4Digits
    handleCardNumber = (text) => {
        let formattedText = text.split(' ').join('');
        if (formattedText.length > 0) {
          formattedText = formattedText.match(new RegExp('.{1,4}', 'g')).join(' ');
        }
        this.setState({ cardNumber: formattedText });
      }

    render(){
        return(
            <View style={{ flex: 1 }}>
              <SafeAreaView style={{ flex: 1}}>
                <View style={{flex: .08, backgroundColor:'#fff'}}>
                    <Header
                    leftNavigation={this.props.navigation}
                    color={this.props.navigation.state.params.color}
                    value={'Payment Method'}
                    />
                </View>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{flex: 1}}
                keyboardVerticalOffset={Platform.select({ios: -80, android: 20})}
                >
                <View style={{ flex: 1}}>
                    
                    <View style={{ marginVertical: 20, marginHorizontal: 10, flex:1,}}>
                        <View style={[styles.Card, { flex: 1 }]}>
                            <View style={{ marginHorizontal:10, flex: 1 }}>
                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center'}}>
                                    <LightText style={{ color: this.props.navigation.state.params.color }}>CARD NUMBER</LightText>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center'}}>
                                    <TextInput
                                        style={{ flex: 1, backgroundColor:colors.gray01, height: 40, borderRadius: 5, paddingLeft: 10, fontSize:16, alignItems:"flex-start", color:colors.patient, marginLeft: 10}} 
                                        placeholder ="7212 **** **** *741"
                                        keyboardType={'phone-pad'}
                                        value={this.state.cardNumber}
                                        onChangeText={(cardNumber) => this.handleCardNumber(cardNumber)}
                                    />
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center'}}>
                                    <View style={{ flex: 3}}>
                                    <LightText style={{  color: this.props.navigation.state.params.color }}>EXPIRATION DATE</LightText>
                                    </View>
                                    <View style={{ flex: 1}}>
                                    <LightText style={{ color: this.props.navigation.state.params.color }}>CVC</LightText>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent:'space-around'}}>
                                    <TouchableOpacity style={{flexDirection: 'row', backgroundColor:colors.gray01, height: 40, borderRadius: 5, paddingHorizontal: 10, alignItems:'center', justifyContent:'space-around'  }}>
                                        <LightText>02</LightText>
                                        <Image
                                        style={{ width: 15, marginHorizontal: 5}}
                                            source = { require('../assets/down-arrow.png')}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{flexDirection: 'row', backgroundColor:colors.gray01, height: 40, borderRadius: 5, paddingHorizontal: 10, alignItems:'center', justifyContent:'space-around'  }}>
                                    <LightText>2002</LightText>
                                        <Image
                                        style={{ width: 15, marginHorizontal: 5}}
                                            source = { require('../assets/down-arrow.png')}
                                        />
                                    </TouchableOpacity>
                                    <View style={{flexDirection: 'row', backgroundColor:colors.gray01, height: 40, width: 80, borderRadius: 5, paddingHorizontal: 10, alignItems:'center'}}>
                                    <TextInput
                                        style={{ flex: 1, backgroundColor:colors.gray01, height: 40, borderRadius: 5, paddingLeft: 10, fontSize:16, alignItems:"flex-start", color:colors.patient, marginLeft: 10}} 
                                        placeholder ="***"
                                        keyboardType={'phone-pad'}
                                        maxLength={3}
                                        secureTextEntry={true}
                                    />
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{ marginVertical: 10, marginHorizontal: 10, flex:1}}>
                        <Button
                            value={'Continue'}
                            color={this.props.navigation.state.params.color}
                            onPress={()=> {
                                this.onContinue()
                                // (this.props.navigation.state.params.color === colors.patient) ? (this.props.navigateToPatientTab(this.props.navigation)) : (this.props.navigateToFamilyMemberTab(this.props.navigation))
                            }}
                            />
                            <View style={{ alignItems:'center'}}>
                            <LightText style={{ textAlign: 'center' }}> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </LightText>
                            </View>
                    </View>

                </View>
                </KeyboardAvoidingView>
             </TouchableWithoutFeedback>
             </SafeAreaView>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    Card:{
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        shadowColor: '#451B2D',
        shadowOffset: {width: 0, height: 9},
        shadowOpacity: 0.18,
        shadowRadius: 21,
    }
});
const mapStateToProps = state =>{
    return{
        isEdit: state.user.isEdit
    }
};
const mapDispatchToProps = dispatch =>{
    return{
        navigateToFamilyMemberTab:(navigation) => navigateToFamilyMemberTab(navigation),
        navigateToPatientTab:(navigation) => navigateToPatientTab(navigation),
        editProfile:() =>{
            dispatch({
                type: EDIT_PROFILE_SUCCESS,
            });
           },
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Payment);