import React from "react";
import {View, Text, Dimensions,TextInput,Image,Platform} from 'react-native';
import { ChatManager, TokenProvider, } from '@pusher/chatkit-client';
import { connect } from 'react-redux';
// import { GetChatingUsers, FindScanfitContacts } from "../../../store/actions/user";
// import { CreateNewRoom , ChatRoomLeft} from "../../../store/actions/chat";
import { GiftedChat } from "react-native-gifted-chat";
import colors from '../styles/colors';
// import BottomBorderView from '../../../Component/BottomBorderView';
    const CHATKIT_TOKEN_PROVIDER_ENDPOINT = 'https://us1.pusherplatform.io/services/chatkit_token_provider/v1/83ad1418-db27-4649-93c8-4d165ce52fcd/token';
    const CHATKIT_INSTANCE_LOCATOR = 'v1:us1:83ad1418-db27-4649-93c8-4d165ce52fcd';
class MyChat extends React.Component {
      state = {
        messages: [],
        otherParty:null,
        otherPartPresence:'offline',
        receiverid:null
      };

      componentWillUnmount(){
        // this.props.ChatRoomLeft();
      }
     async componentDidMount() {
      var currentRoom =null;
    var userIDCurrent = this.props.userData.phone_number;
    // alert(userIDCurrent)
    if(this.props.userData.userType === "ServiceProvider"){
       currentRoom = this.props.SPChatData.id;
      console.log("------------------------",this.props.SPChatData)
    }
    
    // alert(currentRoom);
        // var user =  await this.props.navigation.getParam('HomeView')
        // console.log("user name",this.props.navigation.getParam('data'));
        this.setState({
            otherParty:this.props.navigation.getParam('data'),
            otherimage:this.props.navigation.getParam('picOther')
        })
        const tokenProvider = new TokenProvider({
          url: CHATKIT_TOKEN_PROVIDER_ENDPOINT,
        });
    
        const chatManager = new ChatManager({
          instanceLocator: CHATKIT_INSTANCE_LOCATOR,
          userId:userIDCurrent ,
          tokenProvider: tokenProvider,
        });
    
        chatManager
          .connect()
          .then(currentUser => {
             this.currentUser = currentUser;
            this.currentUser.subscribeToRoom({
              roomId: currentRoom,
              hooks: {
                // onMessage: message => (alertmessage.text),
                onMessage: this.onReceive,
                 onPresenceChanged: this.onchangePresence,
              },
            });
          })
          .catch(err => {
            // alert(JSON.stringify(err))
            this.props.navigation.pop();
            console.log('error', err);
          });
      }

      onchangePresence= async(state,user)=>{
       var presenceStoreLocal =user.presenceStore;
        var listuser = await Object.keys(user.presenceStore)
         console.log(user)
        if(listuser[0]!==this.props.userData.phone_number){
          var user=listuser[1];
          var statusme= (presenceStoreLocal[user])
          this.setState({receiverid:listuser[0],otherPartPresence:statusme},()=>{
            console.log(this.state)
          })
        }else{
          console.log(listuser[1])
          var user=listuser[1];
          var statusme= (presenceStoreLocal[user])
          this.setState({receiverid:user,otherPartPresence:statusme},()=>{
            console.log(this.state)
          })
        }
       
      }
      onReceive = data => {
         // console.log("data", data);
        const { id, senderId, text, createdAt } = data;
        const incomingMessage = {
          _id: id,
          text: text,
          createdAt: new Date(createdAt),
          user: {
            _id: senderId,
            name: senderId,
            avatar:this.state.otherimage
          },
        };
    
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, incomingMessage),
        }));
      };
      onSend = (messages = []) => {
        var currentRoom = this.props.SPChatData.id

        messages.forEach(message => {
          this.currentUser
            .sendMessage({
              text: message.text,
              roomId: currentRoom,
            })
            .then(() => {})
            .catch(err => {
              console.log(err);
            });
        });
      };

      render() {
        var userIDCurrent = this.props.userData.phone_number
        return (
          <View style={{flex:1}}>
             <View style={{flex: .08, backgroundColor:'#fff'}}>
         <Header
          leftNavigation={this.props.navigation}
          color={colors.patient}
          value={'Appoinment'}
        />
        </View>
       
        {/* <BottomBorderView horizontal={0} top={10}/> */}
                <GiftedChat messages={this.state.messages} 
        onSend={messages => this.onSend(messages)}
             user={{
               _id: userIDCurrent
             }}
        />
          </View>
        );
      }
    }
    const mapStateToProps = state =>{
      return{
          Contacts: state,
          chatLoader: state,
          chattingLoading: state,
          userData: state.user,
          RoomID: state,
          SPChatData: state.services.SPChatData
      }
    };
    
    const mapDispatchToProps = (dispatch) =>{
      return{
          // GetChatingUsers:(searchTerm) => GetChatingUsers(searchTerm,dispatch),
          // FindScanfitContacts:(Contacts) => FindScanfitContacts(Contacts,dispatch),  
          // CreateNewRoom:(body,name,navigation) => CreateNewRoom(body,name,navigation,dispatch) ,
          // ChatRoomLeft:()=>ChatRoomLeft(dispatch)
      }
    };
    export default connect(mapStateToProps, mapDispatchToProps)(MyChat);