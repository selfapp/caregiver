import React, { Component } from "react";
import {
  Image,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../styles/colors";
import { connect } from 'react-redux';
import { selectUserType } from './../store/actions/user';
import {BoldText} from './../Components/styledTexts';

 class ProfileType extends Component {
  constructor() {
    super();
    this.state = {

    };
  }
  componentWillMount() {
  }

  componentWillUnmount() {
  }

  
  render() {
    return (
     <View style={{ flex: 1 }}>
        <View style={{ flex: 1, alignItems:'center', justifyContent:'center', marginTop:11 }}>
              <BoldText style={{fontSize:20,color:colors.lightBlack}}>I am a...</BoldText>
         </View>
         <View style={{ flex: 9 }}>
            <TouchableOpacity style={{ flex: 1, flexDirection: 'row', backgroundColor: colors.patient}}
              onPress={() => this.props.selectUserType('Patient',this.props.navigation)}
             >
                <View style={{ flex: 1, alignItems:'center', justifyContent:'center' }}>
                  <Image
                    source={require("../../src/assets/patient.png")}
                  />
                </View>
                <View style={{ flex: 2, justifyContent:'center' }}>
                  <BoldText style={{ color: colors.white}}>Patient</BoldText>
                </View>
                <View style={{ flex: 1, alignItems:'flex-end' }}>
                  <Image
                    style={{ flex: 1 }}
                    source={require("../../src/assets/patient-side-img.png")}
                    resizeMode={'stretch'}
                  />
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, flexDirection:'row', backgroundColor: colors.familyMember}}
              onPress={() => this.props.selectUserType('family' ,this.props.navigation)}
            >
             <View style={{ flex: 1, alignItems:'center', justifyContent:'center' }}>
                  <Image
                    source={require("../../src/assets/family-member.png")}
                  />
                </View>
                <View style={{ flex: 2, justifyContent:'center' }}>
                  <BoldText style={{ color: colors.white}}>Family Member</BoldText>
                </View>
                <View style={{ flex: 1, alignItems:'flex-end' }}>
                  <Image
                    style={{ flex: 1 }}
                    source={require("../../src/assets/patient-side-img.png")}
                    resizeMode={'stretch'}
                  />
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, flexDirection:'row', backgroundColor: colors.serviceProvider}}
                onPress={() => this.props.selectUserType('ServiceProvider' ,this.props.navigation)}
            >
             <View style={{ flex: 1, alignItems:'center', justifyContent:'center' }}>
                  <Image
                    source={require("../../src/assets/service-provider.png")}
                  />
                </View>
                <View style={{ flex: 2, justifyContent:'center' }}>
                  <BoldText style={{ color: colors.white}}>Service Provider</BoldText>
                </View>
                <View style={{ flex: 1, alignItems:'flex-end' }}>
                  <Image
                    style={{ flex: 1 }}
                    source={require("../../src/assets/patient-side-img.png")}
                    resizeMode={'stretch'}
                  />
                </View>
            </TouchableOpacity>
         </View>
     </View>   
     
    );
  }
}


const mapStateToProps = state =>{
  return {
      
  }
};

const mapDispatchToProps = dispatch =>{
  return{
    selectUserType:(userType, navigation) => selectUserType(userType, dispatch, navigation)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileType);