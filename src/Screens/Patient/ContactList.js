import React,{Component} from 'react';
import {
    View,
    SafeAreaView,
    FlatList,
    Image,
    TextInput,
    StyleSheet,
    Platform,
    PermissionsAndroid,
    Share
} from  'react-native';
import { connect } from 'react-redux';
import {navigateToPayment, syncContacts} from '../../store/actions/user';
import {LightText, BoldText} from '../../Components/styledTexts';
import colors from "../../styles/colors";
import Header from '../../Components/Header';
import {Button} from '../../Components/button';
import Contacts from 'react-native-contacts';


class ContactList extends Component {
    constructor() {
      super();
      this.state = {
        searchText:'',
        contacts:[],
        contactList: []
      };
    }

    async componentWillMount() {
        if (Platform.OS === "android") {
          PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
            title: "Contacts",
            message: "This app would like to view your contacts."
          }).then(() => {
            this.loadContacts();
          });
        } else {
          this.loadContacts();
        }
      }

    
    setSearchText(event) {
        let searchText = event.nativeEvent.text;
        this.setState({searchText});
        this.search(searchText)

    }
    loadContacts() {
        Contacts.getAll((err, contacts) => {
          if (err === "denied") {
            alert("Permission to access contacts was denied");
          } else {
            this.setState({ contacts });
            console.log(JSON.stringify(contacts))
            this.filterContacts(contacts)
          }
        });
      }

      filterContacts= async (rawData)=>{
        
 await this.selectContacts(rawData,(data)=>{
 this.props.syncContacts(data)
 // /chatkit/get-sf-users-from-contacts
 })
    }
    selectContacts=async(data,callBack)=>{
        arrayOfContacts=[]
        for (let i = 0; i <  data.length; i++) {
            for (let j = 0; j < data[i].phoneNumbers.length; j++) {
               arrayOfContacts.push(data[i].phoneNumbers[j].number);
            }
        }
        callBack(arrayOfContacts);
    }

      search(text) {
        const newData = this.props.contactList.filter(function(item) {
          //applying filter for the inserted text in search bar
          const itemData = item.number ? item.number.toUpperCase() : ''.toUpperCase();
          const textData = text.toUpperCase();
          return itemData.indexOf(textData) > -1;
        });
     
        this.setState({
          //setting the filtered newData on datasource
          //After setting the data it will automatically re-render the view
          contactList: newData,
          text: text,
        });
      }

      onShare = async () => {
        try {
          const result = await Share.share({
            message:
              'You must be so proud of yourself, share CareGiver with your friends to give them happiness',
          });
    
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
      };
        

    render(){
        return(
            <View style={{ flex: 1}}>
                <SafeAreaView style={{ flex: 1}}>
{/* Header */}
                 <View style={{flex: .08, backgroundColor:'#fff'}}>
                     <Header
                        leftNavigation={this.props.navigation}
                        color={colors.patient}
                        value={'Contact List'}
                     />
                </View>
                <View style={{ height: 70, }}>
                    <View style={{ flex: 1 , marginHorizontal: 20, justifyContent:'center'}}>
                    <View style={[ styles.searchBar,{ flexDirection: 'row', alignItems:'center'}]}>
                        <Image
                            source={require('../../assets/search.png')}
                            style={{ height: 20, width: 20, tintColor: 'gray'}}
                        />
                    <TextInput
                         style={{ flex:1, paddingLeft: 5}}
                        value={this.state.searchText}
                        onChange={this.setSearchText.bind(this)}
                        placeholder={'Type a name or number'}
                        clearButtonMode={"always"}
                         />
                         </View>
                    </View>
                </View>

                <View style={{ flex: 1 }}>
                        <FlatList
                                data={(this.state.searchText.length > 0) ? this.state.contactList : this.props.contactList}
                                renderItem={({item}) =>(
                                    <View>
                                        <View style={{ height: 1, backgroundColor: colors.gray05, marginHorizontal: 10 }}/>
                                    <View style={{ flexDirection: 'row', padding: 10}}>
                                        {
                                            item.image != null ?
                                            <Image
                                            source={{uri:item.image}}
                                            style={{ height: 50, width: 50}}
                                        />
                                            :
                                            <Image
                                            source={require("../../assets/patient.png")}
                                            // source={item.thumbnailPath}
                                            style={{ height: 50, width: 50}}
                                        />
                                        }
                                       
                                        <View style={{ marginLeft: 5, flex: 2 }}>
                                          { (item.first_name != null)?(
                                            <BoldText style={{ fontSize: 16}}>{item.first_name + " "+ item.last_name}</BoldText>):(null)
                                          }
                                          {/* { item.phoneNumbers.map(num=>( */}
                                            <LightText>{item.number}</LightText>
                                           {/* )) } */}
                                        </View>
                                        <View style={{ flex: 1, alignItems:'flex-end', justifyContent: 'center'}}>
                                            { (item.status === "1") ? (
                                                <Image
                                                    source={require("../../assets/check.png")}
                                                    style={{ height: 30, width: 30, tintColor: colors.patient}}
                                                />
                                            ) : (
                                            <Button
                                                Light={true}
                                                style={{ height: 30, width: 80, marginHorizontal: 0, marginVertical:0}}
                                                value={"Invite"}
                                                color={colors.patient}
                                                 onPress={()=> this.onShare()}
                                                />
                                            )}
                                        
                                        </View>
                                    </View>
                                    </View>
                                )}
                        />
                </View>
{/* Continue Button */}
                    <View>
                    <Button
                    value={'Continue'}
                    color={colors.patient}
                    onPress={()=> this.props.navigateToPayment(colors.patient, this.props.navigation)}
                    />
                    </View>

                </SafeAreaView>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    searchBar: {
        paddingLeft: 10,
        fontSize: 16,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#FFFFFF',
        shadowColor: '#451B2D',
        shadowOffset: {width: 0, height: 5},
        shadowOpacity: 0.1,
        shadowRadius: 21,
      }
});

const mapStateToProps = state =>{
    return{
      contactList: state.user.contactList
    }
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
        syncContacts:(contacts)=> syncContacts(contacts, dispatch),
        navigateToPayment:(color, navigation) => navigateToPayment(color, navigation),
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(ContactList);