import React, {Component} from 'react';
import{
View,
SafeAreaView,
}from 'react-native';
import { connect } from 'react-redux';
import Header from '../../Components/Header';
import colors from '../../styles/colors';


class PFeeds extends Component {
 constructor(props){
     super(props)
 }
 render(){
     return(
        <View style={{ flex: 1}}>
            <SafeAreaView style={{ flex: 1}}>
            <View style={{flex: .08, backgroundColor:'#fff'}}>
             <Header
              color={colors.patient}
              value={'Feeds'}
            />
            </View>
            <View style={{ flex: 1}}>

            </View>
            </SafeAreaView>
         </View>
     )
 }
}

const mapStateToProps = state =>{
    return{}
  };
  
  const mapDispatchToProps = dispatch =>{
    return{
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(PFeeds);