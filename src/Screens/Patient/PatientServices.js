import React, { Component } from "react";
import{
    View,
    ScrollView,
    TouchableOpacity,
    Image,
    FlatList,
    SafeAreaView
} from 'react-native';
import {connect } from 'react-redux';
import {
    navigateToContactList,
    selectService,
    updatePatientProfile
} from '../../store/actions/user';
import colors from '../../styles/colors';
import Loader from '../../Components/Loader';
import { BoldText, LightText} from '../../Components/styledTexts';
import Header from '../../Components/Header';
import { Button, OptionButton} from '../../Components/button';

const servicesData = [
    {
        name:'Eating',
        id:1
    },
    {
        name:'Bathing',
        id:2
    },
    {
        name:'Toileting',
        id:3
    },
    {
        name:'Dressing',
        id:4
    },
    {
        name:'Walking or moving about',
        id:6
    },
    {
        name:'Transferring from chair to Bed or vice versa',
        id:5
    },
    
]


class PatientServices extends Component{
    constructor(props){
        super(props);
        this.state ={
            service: 1
        }
    }
    render(){
        return(
            <View style={{ flex: 1}}>
                <SafeAreaView style={{ flex: 1}}>
{/* Header */}
                 <View style={{flex: .08, backgroundColor:'#fff'}}>
                     <Header
                        leftNavigation={this.props.navigation}
                        color={colors.patient}
                        value={'Profile'}
                     />
                </View>
                <ScrollView style={{ flex: 1}}>
                    <View style={{marginHorizontal: 10}}>
{/* Services  */}
                    <View style={{ marginVertical: 25 }}>
                            <View style={{ flexDirection:'row', marginHorizontal: 15, alignItems:'center'}}>
                                <Image
                                    source={require("../../assets/flag.png")}
                                    resizeMode={'contain'}
                                    style={{ height: 20, width: 20, tintColor: colors.patient }}
                                />
                                <LightText style={{ color: colors.darkBlue, marginLeft: 10, fontSize: 16}}>Services</LightText>
                            </View>
                                <View style={{flex:1,flexDirection:'row', flexWrap:'wrap'}}>
                                {/* <FlatList
                                style={{ marginTop: 20, marginHorizontal:20 }}
                                data={servicesData}
                                numColumns={2}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item, index}) =>( */}
                                {
                                    servicesData.map((item,index)=>{
                                        return<OptionButton
                                        style={{ padding:5,}}
                                        value={item.name}
                                        active= {(this.props.services === item.id) ? true : false}
                                        color={colors.patient}
                                        onPress={ () => this.props.selectService(item.id)}
                                    />
                                    })
                                }
                                    {/* <View style={{ padding: 10}}>
                                       
                                </View> */}
                                {/* )}
                                /> */}
                                </View>
                            </View>
                            <View style={{ height: 1, backgroundColor: colors.gray05}}/>
{/* Invite Your Family/Friend      */}

                        <View style={{ marginVertical: 25 }}>
                            <View style={{ flexDirection:'row', marginHorizontal: 15, alignItems:'center'}}>
                                <Image
                                    source={require("../../assets/heart.png")}
                                    resizeMode={'contain'}
                                      style={{ height: 20, width: 20, tintColor: colors.patient }}
                                />
                                <LightText style={{ color: colors.darkBlue, marginLeft: 10, fontSize: 16}}>Invite your Family/Friend</LightText>
                            </View>
                            <View style={{ alignItems: 'center', marginVertical: 20 }}>
                                <TouchableOpacity style={{ padding: 8, borderStyle: 'dotted', borderWidth: 1, borderRadius: 1,}}
                                onPress={() => this.props.navigateToContactList(this.props.navigation)}
                                >
                                        <LightText style={{ color: colors.darkBlue, marginLeft: 10, fontSize: 16}}> Click to Invite your Family/Friend</LightText>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <FlatList
                                data={servicesData}
                                renderItem={({item}) =>(
                                    <View>
                                        <View style={{ height: 1, backgroundColor: colors.gray05, marginHorizontal: 10 }}/>
                                    <View style={{ flexDirection: 'row', padding: 10}}>
                                        <Image
                                            source={require("../../assets/patient.png")}
                                            style={{ height: 50, width: 50}}
                                        />
                                        <View style={{ marginLeft: 5, flex: 2 }}>
                                            <BoldText style={{ fontSize: 16}}>Dick Costolo </BoldText>
                                            <LightText>1-774 5534</LightText>
                                        </View>
                                        <Image
                                            source={require("../../assets/check-circle.png")}
                                            style={{ height: 30, width: 30, tintColor: colors.patient}}
                                        />
                                    </View>
                                    </View>
                                )}
                                />
                            </View>
                        </View>
{/* Continue button */}
                        <View>
                            <Button
                                value={'Continue'}
                                color={colors.patient}
                                onPress = {() => this.props.updatePatientProfile(this.props.profile_picture, this.props.first_name, this.props.last_name, this.props.gender, this.props.age_group, this.props.service, this.props.navigation)}
                                //  onPress={()=> this.props.navigateToPayment(colors.patient, this.props.navigation)}
                                />
                            </View>
                    </View>
                    <Loader loading={this.props.loading} />
                </ScrollView>
                </SafeAreaView>
            </View>
        )
    }
}

const mapStateToProps = state =>{
    return{
        first_name: state.user.first_name,
        last_name: state.user.last_name,
        gender: state.user.gender,
        age_group: state.user.age_group,
        service: state.user.service,
        profile_picture: state.user.profile_image,
    }
};
const mapDispatchToProps = dispatch =>{
    return{
        selectService: (service_id) => {
            dispatch(selectService(service_id));
          },
        updatePatientProfile:(profile_image, first_name, last_name, gender, age_group, service, navigation) => updatePatientProfile(profile_image, first_name, last_name, gender,age_group, service,dispatch, navigation),
        navigateToContactList:(navigation) => navigateToContactList(navigation)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(PatientServices);