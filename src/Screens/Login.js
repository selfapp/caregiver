import React, { Component } from "react";
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  Alert,
  TextInput,
  BackHandler,
  View,
  ImageBackground,
  TouchableWithoutFeedback,
  Keyboard
} from "react-native";
import { connect } from 'react-redux';
import {
  LightText
} from './../Components/styledTexts';
import Loader from './../Components/Loader';
import {Button} from '../Components/button';
import { typeMobile, chooseCountry, login } from '../store/actions/user';
import colors from "../styles/colors";



class Login extends Component {
  constructor() {
    super();
    this.state = {
      touchCount:0
    };
  }

  handleClick = () => {
    this.props.login(this.props.country_code, this.props.phone_number, this.props.navigation.navigate)
    // this.props.navigation.navigate("VerificationCode");
  };

  
  handleBackButton(BackHandler) {
    BackHandler.exitApp();
    return true;
  }
  backPressed = () => {
    Alert.alert(
      "Exit App",
      "Do you want to exit?",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => BackHandler.exitApp() }
      ],
      { cancelable: false }
    );
    return true;
  };

  componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  render(){
    return(
        <View style = {{ flex: 1}}>
        <ImageBackground
              source={require("../../src/assets/bg-img.png")}
              resizeMode="cover"
              style={{ flex: 1,backgroundColor:colors.white }}
            >
         <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <KeyboardAvoidingView
             behavior={Platform.OS === "ios" ? "padding" : null}
             style={{flex: 1}}
             keyboardVerticalOffset={Platform.select({ios: -60, android: 20})}
            >
                <View style={{ flex: 0.3 }}/>
                <View style={{flex:1, alignItems:'center', justifyContent:'space-evenly' }}>
                    <TouchableWithoutFeedback
                    onPress = {() =>{
                      this.setState({touchCount: this.state.touchCount + 1})
                      if(this.state.touchCount > 2){
                        this.props.chooseCountry('+91');
                        alert('country chane to india')
                      }
                    }}
                    >
                    <Image
                      source={require("../../src/assets/logo-icon.png")}
                      style={{ alignItems:'center'}}
                    />
                    </TouchableWithoutFeedback>
                </View>

                <View style={{flex: 1.5, marginHorizontal: 20, justifyContent:'center'}}>
                    <LightText style={{ marginLeft: 10 }}>Mobile No.</LightText>
                    <View style={[{ borderWidth: 1, borderColor: 'transparent'}, this.props.errors.phone_number ? styles.errorInput : {}]}>
                            
                            <View style={[{flexDirection: 'row', alignItems: 'center'}]}>
                                <TextInput
                                    maxLength={10}
                                    underlineColorAndroid='transparent'
                                    autoCorrect={false}
                                    returnKeyType='done'
                                    keyboardType='numeric'
                                    placeholder="Enter your mobile no"
                                    placeholderTextColor='rgb(119,120,122)'
                                    fontSize={20}
                                    value={this.state.PhoneNumber}
                                    onChangeText={(phone_number) => this.props.phoneNumberChange(phone_number)}
                                    style={[styles.normalInput]}
                                />
                            </View>
                            <View style={{height: 1, backgroundColor: 'rgb(119,120,122)'}}/>
                        </View>
                        <LightText style={{ marginTop: 2, color: 'red' }}>{this.props.errors.phone_number ? this.props.errors.phone_number[0] : null}</LightText>

                        
                        <View style={{flex:1}}>
                          <View style={{flex: 1, justifyContent: 'center'}}>
                            <Button
                                value={'Login'}
                                color={colors.colorSignin}
                                onPress={() => this.props.login(this.props.country_code, this.props.phone_number, this.props.navigation.navigate)}
                                />
                            </View>
                        </View>
                        </View>
                        <Loader loading={this.props.loading} />
            </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
            </ImageBackground>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  normalInput: {
    height: 40,
    color: colors.colorSignin,
    marginLeft: 10,
    fontWeight:'bold',
    flex: 1
},
errorInput: {
    borderColor: 'red'
}
});


const mapStateToProps = state =>{
  return{
      phone_number: state.user.phone_number,
      country_code: state.user.country_code,
      loading: state.user.loading,
      errors: state.user.errors
  }
};

const mapDispatchToProps = dispatch =>{
  return{
      phoneNumberChange: (phone_number) => {
          dispatch(typeMobile(phone_number));
      },
      chooseCountry: (country_code) => {
        dispatch(chooseCountry(country_code));
    },
      login:(country_code, phone_number,navigate) => login(country_code, phone_number,dispatch, navigate)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);