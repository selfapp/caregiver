import React, {Component} from 'react';
import { StyleSheet}from 'react-native';
export default StyleSheet.create({
    Card: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        shadowColor: 'gray',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.18,
        shadowRadius: 18,
        width: '100%',
        elevation: 5
      },
});