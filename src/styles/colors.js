export default {
    black: "#000000",
    lightBlack: "#484848",
    white: "#ffffff",
    green01: "#008388",
    green02: "#02656b",
    green03:"#417505",
    darkOrange: "#d93900",
    lightGray: "#d8d8d8",
    pink: "#fc4c54",
    gray01: "#f3f3f3",
    gray02: "#919191",
    gray03: "#b3b3b3",
    gray04: "#484848",
    gray05: "#dadada",
    gray06: "#ebebeb",
    gray07: "#f2f2f2",
    brown01: "#ad8763",
    brown02: "#7d4918",
    colorSignin: "#293340",
    colorTwitter: "#5AB4EF",
    colorFacebook: "#44619D",
    blue: "#4995cd",
    darkBlue:"#1e60b0",
    colorClipTop: "#C02313",
    colorClipBottom: "#D38227",
    colorGradient: "#F76B1C",
    colorClockTime: "#F6F6F6",
    cloorClockTimeBottom: "#9A4038",
    colorDrawer: "#686766",
    patient: '#0462b0',
    familyMember: '#fc5d63',
    serviceProvider: '#247E6C'
  };
  