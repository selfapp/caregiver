import React, {Component} from "react";
import {
  SafeAreaView,Alert,AsyncStorage
} from 'react-native';
import {Provider} from 'react-redux';
import configureStore from './src/store/configureStore';
import { AppContainer} from './router';
import firebase from 'react-native-firebase';

const store = configureStore();
window.store =store;
export default class App extends Component {
  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners();
  }
  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }
  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
   this.notificationListener = firebase.notifications().onNotification((notification) => {
    const { title, body } = notification;
    console.log('notificationOpen.notification',notification)
    console.log('notification ios',notification.ios)
    this.showAlert(title, body);
});
  
    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
   this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    const { title, body } = notificationOpen.notification;
    console.log('notificationOpen.notification',notificationOpen.notification)
    this.showAlert(title, body);
});
  
    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      console.log('notificationOpen.notification',notificationOpen.notification)
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
  }
    /*
    * Triggered for data only payload in foreground
    * */
   this.messageListener = firebase.messaging().onMessage((message) => {
    //process data message
    console.log(JSON.stringify(message));
  });
}
  
  showAlert(title) {
    Alert.alert(
      'CareGiver',
      title,
      [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }
  


  //1
async checkPermission() {
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
      this.getToken();
  } else {
      this.requestPermission();
  }
}

  //3
async getToken() {
  let fcmToken = await AsyncStorage.getItem('fcmToken');
  if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
          // user has a device token
          await AsyncStorage.setItem('fcmToken', fcmToken);
          console.log("fcm Tokem",fcmToken);
      }
  }
}

  //2
async requestPermission() {
  try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
  } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
  }
}
  render() {    
      return (
        <Provider store={store}>
          {/* <SafeAreaView style={{flex: 1, backgroundColor:'#fff'}}> */}
           <AppContainer />
          {/* </SafeAreaView> */}
        </Provider>
      ); 
  }
}
