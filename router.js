import React from "react";
import{
  Image
} from 'react-native';
import { 
  createAppContainer, 
  createStackNavigator, 
  createBottomTabNavigator
} from 'react-navigation';
import colors from './src/styles/colors';
import Splash from './src/Screens/Splash';
import Login from "./src/Screens/Login";
import VerificationCode from "./src/Screens/VerificationCode";
import ProfileType from "./src/Screens/ProfileType";
import ChatScreen from "./src/Screens/Chat"
//Patient
import PatientProfile from "./src/Screens/Patient/PatientProfile";
import PatientServices from './src/Screens/Patient/PatientServices';
import PServices from './src/Screens/Patient/PServices';
import PFeeds from './src/Screens/Patient/PFeeds';
import PProfile from './src/Screens/Patient/PProfile';
import ContactList from './src/Screens/Patient/ContactList';

// Appoinments
import AppoinmentHome from './src/Screens/Appoinment/AppoinmentHome';
import AppoinmentDetail from './src/Screens/Appoinment/AppoinmentDetail';

//Family member
import FamilyProfile from './src/Screens/FamilyMember/FamilyProfile';
import FamilyDetails from './src/Screens/FamilyMember/FamilyDetails';
import AddFMember from './src/Screens/FamilyMember/AddFMember';
import AddPatient from './src/Screens/FamilyMember/AddPatient';
import FMPatientList from './src/Screens/FamilyMember/FMPatientList';
import FMFeeds from './src/Screens/FamilyMember/FMFeeds';
import FMProfile from './src/Screens/FamilyMember/FMProfile';
import BookAppointment from './src/Screens/FamilyMember/BookAppointment';
import FMLocation from './src/Screens/FamilyMember/FMLocation';

//serviceProvider
import ServiceProviderProfile from './src/Screens/ServiceProvider/ServiceProviderProfile';
import ServiceProviderDetails from './src/Screens/ServiceProvider/ServiceProviderDetails';
import SPHome from './src/Screens/ServiceProvider/SPHome';
import SPServices from './src/Screens/ServiceProvider/SPServices';
import SPProfile from './src/Screens/ServiceProvider/SPProfile';
import SPNotification from './src/Screens/ServiceProvider/SPNotification';
import SPFeeds from './src/Screens/ServiceProvider/SPFeeds';
import tripDetails from './src/Screens/ServiceProvider/tripDetails'

import Payment from './src/Screens/Payment';


export const loginNavigationOptions = createStackNavigator(
  {
    Login:{
          screen: Login
      },
      VerificationCode:{
          screen: VerificationCode
      }
      },{
      headerMode: 'none'
      }
  );

export const profileCreationNavigationOptions = createStackNavigator({
  ProfileType:{
    screen: ProfileType
  },
  PatientProfile:{
    screen: PatientProfile
  },
  PatientServices:{
    screen: PatientServices
  },
  ContactList:{
    screen: ContactList
  },
  FamilyProfile:{
    screen: FamilyProfile
  },
  FamilyDetails:{
    screen: FamilyDetails
  },
  AddFMember:{
    screen: AddFMember
  },
  AddPatient:{
    screen: AddPatient
  },
  ServiceProviderProfile:{
    screen: ServiceProviderProfile
  },
  ServiceProviderDetails:{
    screen: ServiceProviderDetails
  },
  Payment:{
    screen: Payment
  },
  ChatScreen:{
    screen:ChatScreen
  }
  },{
    headerMode: 'none'
  }
);

// Edit patient profile
export const EditPatientProfile = createStackNavigator({
  PProfile: { screen: PProfile},
  PatientProfile:{
    screen: PatientProfile
  },
  PatientServices:{
    screen: PatientServices
  },
  ContactList:{
    screen: ContactList
  }, 
  Payment:{
    screen: Payment
  }
},{
  headerMode: 'none'
})

export const SPProfileNavigationOptions = createStackNavigator({
  Profile:{
    screen: SPProfile
  },
  SPServices:{
    screen: SPServices
  }
},
  {
    headerMode:'none'
  }

);
export const SPNotificationoption = createStackNavigator({
  SPNotification:{
    screen: SPNotification
  },
  ChatScreen:{
    screen:ChatScreen
  },
  tripDetails:{
    screen: tripDetails
  }
},
  {
    headerMode:'none'
  }

);


export const EditSPProfile = createStackNavigator({
  SPProfileNavigationOptions: { screen: SPProfileNavigationOptions},
  ServiceProviderProfile:{
    screen: ServiceProviderProfile
  },
  ServiceProviderDetails:{
    screen: ServiceProviderDetails
  },
},{
  headerMode: 'none'
})

// Service Provider Tab

export const SpTabNavigator = createBottomTabNavigator(
  {
      Services:{
          screen: SPHome,
          navigationOptions:{
              tabBarIcon: ({tintColor}) =>{
                  return(
                      <Image
                      source = {require('./src/assets/BottomMenu/service-gray.png')}
                      style = {{tintColor:tintColor}}
                      />
                  )
              }
          }
      },
      Feeds:{
          screen: SPFeeds,
          navigationOptions:{
              tabBarIcon: ({tintColor}) =>{
                  return(
                      <Image
                      source = {require('./src/assets/BottomMenu/feeds.png')}
                      style = {{tintColor:tintColor}}
                      />
                  )
              }
          }
      },
      Notifications:{
          screen: SPNotificationoption,
          navigationOptions:{
              tabBarIcon: ({tintColor}) =>{
                  return(
                      <Image
                      source = {require('./src/assets/BottomMenu/profile-notification.png')}
                      style = {{tintColor:tintColor}}
                      />
                  )
              }
          }
      },
    Profile:{
      screen: EditSPProfile,
      navigationOptions:{
          tabBarIcon: ({tintColor}) =>{
              return(
                  <Image
                  source = {require('./src/assets/BottomMenu/profile.png')}
                  style = {{tintColor:tintColor}}
                  />
              )
          }
      }
  }
  },
  {
      initialRouteName: 'Services',        
      tabBarOptions: {
          activeTintColor: colors.serviceProvider,
          inactiveTintColor: "gray",
          showLabel: true,
          showIcon: true,
          tabBarPosition: 'bottom',
          labelStyle: {
              fontSize: 12,
          },
          iconStyle:{
              width: 30,
              height: 30
          },
          style: {
              backgroundColor: 'rgb(245,245,245)',
              borderBottomWidth: 1,
              borderBottomColor: '#ededed',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
          },
          lazy: true,
          indicatorStyle: '#fff',
      }
  }
);

// Patient Tab

export const Pappoinments = createStackNavigator({
  PAppoinmentHome:{
    screen: AppoinmentHome
  },ChatScreen:{
    screen:ChatScreen
  },
   PAppoinmentDetail:{
    screen: AppoinmentDetail
  },
  
},
  {
    headerMode:'none'
  }

);
export const PTabNavigator = createBottomTabNavigator(
  {
      Services:{
          screen: PServices,
          navigationOptions:{
              tabBarIcon: ({tintColor}) =>{
                  return(
                      <Image
                      source = {require('./src/assets/BottomMenu/service-gray.png')}
                      style = {{tintColor:tintColor}}
                      />
                  )
              }
          }
      },
      Ongoing:{
        screen: Pappoinments,
        navigationOptions:{
            tabBarIcon: ({tintColor}) =>{
                return(
                    <Image
                    source = {require('./src/assets/location-map.png')}
                    style = {{tintColor:tintColor}}
                    />
                )
            }
        }
    },
      Feeds:{
          screen: PFeeds,
          navigationOptions:{
              tabBarIcon: ({tintColor}) =>{
                  return(
                      <Image
                      source = {require('./src/assets/BottomMenu/feeds.png')}
                      style = {{tintColor:tintColor}}
                      />
                  )
              }
          }
      },
      Profile:{
        screen: EditPatientProfile,
        navigationOptions:{
            tabBarIcon: ({tintColor}) =>{
                return(
                    <Image
                    source = {require('./src/assets/BottomMenu/profile.png')}
                    style = {{tintColor:tintColor}}
                    />
                )
            }
        }
    },
    
  },
  {
      initialRouteName: 'Services',        
      tabBarOptions: {
          activeTintColor: colors.patient,
          inactiveTintColor: "gray",
          showLabel: true,
          showIcon: true,
          tabBarPosition: 'bottom',
          labelStyle: {
              fontSize: 12,
          },
          iconStyle:{
              width: 30,
              height: 30
          },
          style: {
              backgroundColor: 'rgb(245,245,245)',
              borderBottomWidth: 1,
              borderBottomColor: '#ededed',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
          },
          lazy: true,
          indicatorStyle: '#fff',
      }
  }
);


export const FMPatientsNavigationOptions = createStackNavigator({
  FMPatientList:{
    screen: FMPatientList
  },
  AddPatient:{
    screen: AddPatient
  },
  BookAppointment:{
    screen:BookAppointment
  },
  FMLocation:{
    screen:FMLocation
  },
},
  {
    headerMode:'none'
  }

);
export const FMappoinments = createStackNavigator({
  AppoinmentHome:{
    screen: AppoinmentHome
  },
   AppoinmentDetail:{
    screen: AppoinmentDetail
  },ChatScreen:{
    screen:ChatScreen
  },
  
},
  {
    headerMode:'none'
  }

);

export const EditFMProfile = createStackNavigator({
  FMProfile: { screen: FMProfile},
  FamilyProfile:{
    screen: FamilyProfile
  },
  FamilyDetails:{
    screen: FamilyDetails
  },
  AddFMember:{
    screen: AddFMember
  },
  Payment:{
    screen: Payment
  }
},{
  headerMode: 'none'
})

// Family member Tab
export const FMTabNavigator = createBottomTabNavigator(
  {
      Patients:{
          screen: FMPatientsNavigationOptions,
          navigationOptions:{
              tabBarIcon: ({tintColor}) =>{
                  return(
                      <Image
                      source = {require('./src/assets/BottomMenu/patient-list.png')}
                      style = {{tintColor:tintColor}}
                      />
                  )
              }
          }
      },
      Feeds:{
          screen: FMFeeds,
          navigationOptions:{
              tabBarIcon: ({tintColor}) =>{
                  return(
                      <Image
                      source = {require('./src/assets/BottomMenu/feeds.png')}
                      style = {{tintColor:tintColor}}
                      />
                  )
              }
          }
      },
      Ongoing:{
        screen: FMappoinments,
        navigationOptions:{
            tabBarIcon: ({tintColor}) =>{
                return(
                    <Image
                    source = {require('./src/assets/location-map.png')}
                    style = {{tintColor:tintColor}}
                    />
                )
            }
        }
    },
      Profile:{
        screen: EditFMProfile,
        navigationOptions:{
            tabBarIcon: ({tintColor}) =>{
                return(
                    <Image
                    source = {require('./src/assets/BottomMenu/profile.png')}
                    style = {{tintColor:tintColor}}
                    />
                )
            }
        }
    },
 
  },
  {
      initialRouteName: 'Patients',        
      tabBarOptions: {
          activeTintColor: colors.familyMember,
          inactiveTintColor: "gray",
          showLabel: true,
          showIcon: true,
          tabBarPosition: 'bottom',
          labelStyle: {
              fontSize: 12,
          },
          iconStyle:{
              width: 30,
              height: 30
          },
          style: {
              backgroundColor: 'rgb(245,245,245)',
              borderBottomWidth: 1,
              borderBottomColor: '#ededed',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
          },
          lazy: true,
          indicatorStyle: '#fff',
      }
  }
);


export const appNavigationOptions = createStackNavigator(
  {
      Splash:{
        screen: Splash
     },
      loginNavigationOptions:{
          screen: loginNavigationOptions
      },
      profileCreationNavigationOptions:{
        screen: profileCreationNavigationOptions
      },
      SpTabNavigator:{
        screen: SpTabNavigator
      },
      PTabNavigator:{
        screen: PTabNavigator
      },
      FMTabNavigator:{
        screen: FMTabNavigator
      }
  },
  {
      headerMode: 'none'
  }
);

export const AppContainer = createAppContainer(appNavigationOptions);

